$(document).ready(function(){
  var id = $('.advanced_faq').attr('id');
  
  if(id)
  {
    $.ajax({
      type: "POST",
      url: "https://zestardshop.com/shopifyapp/advancedfaq/public/faq",
      crossDomain: true,
      data: {'id':id},
      success: function(data) {
        $(".advanced_faq").html(data);
      }
    });      
  }
});
