<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DefaultCssModel extends Model
{
  protected $table = 'default_css';

  protected $fillable =[
    'css_element',
    'css_attr',
    'css_attr_value'
  ];
}
