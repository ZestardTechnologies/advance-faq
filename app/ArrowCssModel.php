<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArrowCssModel extends Model
{
    protected $table = 'arrow_css';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable =[
      'arrow_id',
      'arrow_color',
      'arrow_position',
      'shop_id'
    ];
}
