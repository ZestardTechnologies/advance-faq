<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PublishedTrait;

class FrontendCSS extends Model
{
  use PublishedTrait;

  protected $table = 'tbl_frontend_css';

  protected $fillable =[
    'css_element',
    'css_attr',
    'css_attr_value',
    'shop_id',
  ];

  public function getShopIdColumn()
  {
    return 'shop_id';
  }
}
