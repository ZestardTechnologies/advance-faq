<?php namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;
use App\ShopModel;
use App\FrontendCSS;
use App\DefaultCssModel;
use App\ArrowCssModel;
use App\ArrowsModel;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        View::composer('accordion', function($view)
        {           
            $css_attr_array = array();
            $font_size = array();

            $shop_name = session('shop');
            $shop_find = ShopModel::where('store_name' , $shop_name)->first();
            $frontendCSS = FrontendCSS::where('shop_id' , '=' ,$shop_find->id)->get();
            $arrows_css = ArrowCssModel::where('shop_id' , '=' ,$shop_find->id)->first();
            $arrows = ArrowsModel::where('id' , '=' ,$arrows_css->arrow_id)->first();
            if(count($frontendCSS)>0)
            {
                foreach($frontendCSS as $css) {
                $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
                }
                
                $question_size=$css_attr_array['question']['font-size'];
                $answer_size=$css_attr_array['answer']['font-size'];
                $font_size=array("question"=>$question_size,"answer"=>$answer_size);
            
                $border_color=$css_attr_array['question_panel']['border-color'];
                $border_width=$css_attr_array['question_panel']['border'];
                $border_radius=$css_attr_array['question_panel']['border-radius'];
                $question_border=array("color"=>$border_color,"width"=>$border_width,"radius"=>$border_radius);
                $question_padding=$css_attr_array['question_panel']['padding'];
                $answer_padding=$css_attr_array['answer_panel']['padding'];
                $padding=array("question_panel"=>$question_padding,"answer_panel"=>$answer_padding);
            }
            else{
                $defaultcss = DefaultCssModel::all();
                
                foreach($defaultcss as $css) {
                    $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
                }
                
                $question_size=$css_attr_array['question']['font-size'];
                $answer_size=$css_attr_array['answer']['font-size'];
                $font_size=array("question"=>$question_size,"answer"=>$answer_size);
            
                $border_color=$css_attr_array['question_panel']['border-color'];
                $border_width=$css_attr_array['question_panel']['border'];
                $border_radius=$css_attr_array['question_panel']['border-radius'];
                $question_border=array("color"=>$border_color,"width"=>$border_width,"radius"=>$border_radius);
                $question_padding=$css_attr_array['question_panel']['padding'];
                $answer_padding=$css_attr_array['answer_panel']['padding'];
                $padding=array("question_panel"=>$question_padding,"answer_panel"=>$answer_padding);
            }
            $view->with('font_size', $font_size)->with('border_color',$question_border)->with('padding',$padding)->with('arrow_css',$arrows_css)->with('arrow',$arrows);
            
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}