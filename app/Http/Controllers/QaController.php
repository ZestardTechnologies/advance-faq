<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\QaModel;

use App\TopicModel;

use App\ShopModel;

use DB;

class QaController extends Controller
{
  public function index()
  {
    $quastion_list=array();
    $qa = new QaModel;
    $topic = new TopicModel;
    $topic_list = TopicModel::all();
    $shop_name = session('shop');
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $questions = DB::table('question_answer')->leftJoin('topic', 'question_answer.topic_id', '=', 'topic.topic_id')->select('question_answer.*', 'topic.topic_id','topic.topic_name')->where('question_answer.shop_id' ,'=', $shop_find->id)->get();
    
    foreach($questions as $question)
    {
      $quastion_list [$question->qa_id] = [
              'qa_id' => $question->qa_id,
              'question' => $question->question,
              'answer' => $question->answer,
              'topic_id' => $question->topic_id,
              'topic_name' => $question->topic_name,
              'qa_status' => $question->qa_status,
              'qa_order'=> $question->qa_order,
              'encrypt_id'=> $question->encrypt_id
            ];
    }

    return view('qalist',['topics' => $topic_list,'qa' => $quastion_list,'active' => 'qa']);
  }

  public function add()
  {
    $topic = new TopicModel;
    $topic_list = TopicModel::all();
    return view('addqa',['topics' => $topic_list,'active' => 'qa']);
  }

  public function store(Request $recuestdata)
  {
    $shop_name = session('shop');
    $qa = new QaModel;
    $topiclist = TopicModel::all();
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $this->validate($recuestdata, [
          'question' => 'required',
          'answer' => 'required',
          'topic' => 'required',
      ]);

    $qa->question= $recuestdata['question'];
    $qa->answer= $recuestdata['answer'];
    $qa->topic_id= $recuestdata['topic'];
    $qa->qa_status= $recuestdata['qa_status'];
    $qa->qa_order= $recuestdata['qa_order'];
    $qa->shop_id= $shop_find->id;
    $qa->encrypt_id="";
    $qa->save();
    
    //for update encrypt id
    $last_inserted = $qa->qa_id;
    $qa_encrypt = crypt($last_inserted,"ze");
    $finaly_encrypt = str_replace(['/','.'], "Z", $qa_encrypt);
    $update_question_answer = QaModel::where('qa_id',$last_inserted)->update(['encrypt_id' => $finaly_encrypt]);
    
    $questions = QaModel::all();
      if ( ! $qa->save())
      {
        $notification = array(
            'message' => 'Question Added Succesfully.',
            'alert-type' => 'error'
        );
        return redirect('qalist')->with('notification',$notification);
      }

      $notification = array(
          'message' => 'Question Added Succesfully.',
          'alert-type' => 'success'
      );
      return redirect('qalist')->with('notification',$notification);
  }

  public function destroy($id)
  {
    $del = QaModel::where('encrypt_id', '=', $id)->delete();
    
    $notification = array(
        'message' => 'Question Deleted Succesfully.',
        'alert-type' => 'success'
    );
    return redirect('qalist')->with('notification',$notification);
  }

  public function edit($id){
      $qa = QaModel::where('encrypt_id', '=' , $id)->first();

      if(empty($qa))
      {
        $msg="Record not Deleted";
        abort(404,$msg);
      }

      $topic = new TopicModel;
      $topic_list = TopicModel::all();
      return view('editqa',['topics' => $topic_list,'qa' => $qa])->with('active', 'qa');
  }

  public function update(Request $recuestdata,$id){
      $qa = QaModel::where('encrypt_id', '=' , $id)->first();
      $this->validate($recuestdata, [
            'question' => 'required',
            'answer' => 'required',
            'topic' => 'required',
        ]);
      $qa->question= $recuestdata['question'];
      $qa->answer= $recuestdata['answer'];
      $qa->topic_id= $recuestdata['topic'];
      $qa->qa_status= $recuestdata['qa_status'];
      $qa->qa_order= $recuestdata['qa_order'];
      $qa->save();

      $notification = array(
          'message' => 'Question Updated Succesfully.',
          'alert-type' => 'success'
      );
      return redirect('qalist')->with('notification',$notification);
  }
}
