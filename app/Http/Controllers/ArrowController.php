<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopModel;
use App\ArrowCssModel;
use App\ArrowsModel;

class ArrowController extends Controller
{
    public function index(Request $request) {
        $arrows_changed = ArrowsModel::where('id' , '=' ,$request->id)->first();
        return json_encode($arrows_changed);
    }
}
