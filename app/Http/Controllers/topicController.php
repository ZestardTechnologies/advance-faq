<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TopicModel;
use App\ShopModel;
use App\CategoryModel;
use App\QaModel;
use App\FrontendCSS;
use App\DefaultCssModel;
use App\ArrowCssModel;

class topicController extends Controller
{
  public function index()
  {
    $topic = new TopicModel;
    $shop_name = session('shop');
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $topic_list = TopicModel::all();
    return view('topic',['topics' => $topic_list,'active' => 'topic']);
  }

  public function store(Request $recuestdata)
  {
    $shop_name = session('shop');
    $topic = new TopicModel;
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $this->validate($recuestdata, [
          'topic_name' => 'required',
      ]);

    $topic->topic_name= $recuestdata['topic_name'];
    $topic->topic_status= $recuestdata['topic_status'];
    $topic->topic_order= $recuestdata['topic_order'];
    $topic->shop_id= $shop_find->id;
    $topic->encrypt_id="";
    $topic->save();
    
    //for update encrypt id
    $last_inserted = $topic->topic_id;
    $topic_encrypt = crypt($last_inserted,"ze");
    $finaly_encrypt = str_replace(['/','.'], "Z", $topic_encrypt);
    $update_topic = TopicModel::where('topic_id',$last_inserted)->update(['encrypt_id' => $finaly_encrypt]);
                    
    $notification = array(
        'message' => 'Topic Added Successfully.',
        'alert-type' => 'success'
    );

    return redirect('topic')->with('notification',$notification);
  }

  public function destroy($id)
  {
    $topic = TopicModel::where('encrypt_id', '=' , $id)->first();
    $update_question = QaModel::where('topic_id', $topic->topic_id)->update(['topic_id' => 0]);
    $delete = TopicModel::where('encrypt_id', '=' , $id)->delete();
    $notification = array(
        'message' => 'Topic Deleted Successfully.',
        'alert-type' => 'success'
    );
    return redirect('topic')->with('notification',$notification);
  }

  public function edit($id){
      $topic = TopicModel::where('encrypt_id', '=' , $id)->first();

      if(empty($topic))
      {
        $msg="Record not Deleted";
        abort(404,$msg);
      }

      return view('edittopic',['topic' => $topic])->with('active', 'topic');
  }

  public function update(Request $recuestdata,$id){
      $topic = TopicModel::find($id);
      $topic->topic_name= $recuestdata['topic_name'];
      $topic->topic_status= $recuestdata['topic_status'];
      $topic->topic_order= $recuestdata['topic_order'];
      $topic->save();
      $notification = array(
          'message' => 'Topic Updated Successfully.',
          'alert-type' => 'success'
      );

      return redirect('topic')->with('notification',$notification);
  }
  
	public function dashboard()
	{
		$shop = session('shop');
		if(empty($shop))
		{
			$shop = $_GET['shop'];			
			session(['shop' => $shop]);			
		}
		$shop_find = ShopModel::where('store_name', $shop)->first();
		$questions_array = array();
		
		$topics_array = array();
		$topics = TopicModel::all();
		
		$topic_sorted = $topics->where('topic_status', '=', 0)->sortBy('topic_order');
		
		foreach($topic_sorted as $topic) {
			$topics_array[$topic->topic_id]['questions'] = array();
			$topics_array[$topic->topic_id]['topic_name'] = $topic->topic_name;
			$topics_array[$topic->topic_id]['topic_status'] = $topic->topic_status;
			$question_unsorted = $topic->questions;
			$question_sorted = $question_unsorted->where('qa_status', '=', 0)->sortBy('qa_order');
			foreach($question_sorted as $question)  {
			  $topics_array[$topic->topic_id]['questions'][$question->qa_id] =
				array(
				  'question' => $question->question,
				  'answer' => $question->answer,
				);
			}
		  }
			$new_install = $shop_find->new_install;			
			return view('accordion',['topics_array'=>$topics_array,'questions_array'=>$questions_array,'active'=>'manage', 'new_install' => $new_install]);
	}

  public function frontendCss(Request $request)
  {
    $input = $request->all();
        
    $css_styles = json_decode(html_entity_decode($input['json']), true);
    $arrow_css = json_decode(html_entity_decode($input['arrow_json']), true);
    
    $shop_name = session('shop');
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $shop_id = $shop_find->id;
    
    /*
     *  check of the record for the current shop already exists
     *  if not, then only create the record
     */
    
     if(count($css_styles) > 0)
     {
        // update the record
         foreach($css_styles as $css_key=>$css_array) {

        // if the style already exists,override it


             foreach($css_array as $key=>$value)
             {

             $css_style_existing = FrontendCSS::
             where('shop_id' , '=' , $shop_find->id)
             ->where('css_element', '=', $css_key )
             ->where('css_attr' , '=' , $key)
             ->first();

             if(count($css_style_existing))
             {
               $css_style_existing->update([
                 'css_element' => $css_key,
                 'css_attr' => $key,
                 'css_attr_value' => $value,
                 'shop_id' =>$shop_find->id
                 ]);
             } else {
               // create new record
               FrontendCSS::create([
                 'css_element' => $css_key,
                 'css_attr' => $key,
                 'css_attr_value' => $value,
                 'shop_id' =>$shop_find->id
               ]);
             }

             }
         }
     }
     /*
     *  check of the arrow css for the current shop already exists
     *  if not, then only create the record
     */
    if(count($arrow_css) > 0)
    {
        
        $arrow_style_existing = ArrowCssModel::where('shop_id' , '=' , $shop_id);
        
        if(count($arrow_style_existing))
        {
            $arrow_style_existing->update([
                 'arrow_id' => $arrow_css['arrow_id'],
                 'arrow_color' => $arrow_css['arrow_color'],
                 'arrow_position' => $arrow_css['arrow_position'],
                 'shop_id' =>$shop_find->id
                 ]);
            
        }
        else{
            
            ArrowCssModel::create([
                 'arrow_id' => $arrow_css['arrow_id'],
                 'arrow_color' => $arrow_css['arrow_color'],
                 'arrow_position' => $arrow_css['arrow_position'],
                 'shop_id' =>$shop_find->id
               ]);
        }
    }
  }

  public function loadFrontendCSS()
  {
    $css_attr_array = array();

    $shop_name = session('shop');
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $frontendCSS = FrontendCSS::where('shop_id' , '=' ,$shop_find->id)->get();

    // make an array element wise
    if(count($frontendCSS)>0)
    {
        foreach($frontendCSS as $css) {
            $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
        }

        // array for the css
        $question_font=$css_attr_array['question']['font-family'];
        $answer_font=$css_attr_array['answer']['font-family'];
        $font=array("question"=>$question_font,"answer"=>$answer_font);
    }
    else{
        $defaultcss = DefaultCssModel::all();
                
        foreach($defaultcss as $css) {
            $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
        }
        $question_font=$css_attr_array['question']['font-family'];
        $answer_font=$css_attr_array['answer']['font-family'];
        $font=array("question"=>$question_font,"answer"=>$answer_font);
    }
    return json_encode($css_attr_array);
  }
  
  public function loadFont()
  {
    $css_attr_array = array();

    $shop_name = session('shop');
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $frontendCSS = FrontendCSS::where('shop_id' , '=' ,$shop_find->id)->get();

    // make an array element wise
    if(count($frontendCSS)>0)
    {
        foreach($frontendCSS as $css) {
            $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
        }
    
        // array for the font
        $question_font=$css_attr_array['question']['font-family'];
        $answer_font=$css_attr_array['answer']['font-family'];
        $font=array("question"=>$question_font,"answer"=>$answer_font);
    }
    else 
    {
        $defaultcss = DefaultCssModel::all();
        foreach($defaultcss as $css) {
            $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
        }
    
        // array for the font
        $question_font=$css_attr_array['question']['font-family'];
        $answer_font=$css_attr_array['answer']['font-family'];
        $font=array("question"=>$question_font,"answer"=>$answer_font);
        
    }
    
    return json_encode($font);
  }
  
  public function loadColor()
  {
    $css_attr_array = array();

    $shop_name = session('shop');
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $frontendCSS = FrontendCSS::where('shop_id' , '=' ,$shop_find->id)->get();
    if(count($frontendCSS)>0)
    {
        foreach($frontendCSS as $css) {
            $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
        }
        
        // array for the color and size
        $questionbg_color=$css_attr_array['question_panel']['background'];
        $questionfont_color=$css_attr_array['question']['color'];

        $answerbg_color=$css_attr_array['answer_panel']['background'];
        $answerfont_color=$css_attr_array['answer']['color'];

        $question_color=array("color"=>$questionfont_color,"background"=>$questionbg_color);
        $answer_color=array("color"=>$answerfont_color,"background"=>$answerbg_color);
        $color=array("question"=>$question_color,"answer"=>$answer_color);
        
    }
    else 
    {
        $defaultcss = DefaultCssModel::all();
        
        foreach($defaultcss as $css) {
            $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
        }
        
        // array for the color and size
        $questionbg_color=$css_attr_array['question_panel']['background'];
        $questionfont_color=$css_attr_array['question']['color'];

        $answerbg_color=$css_attr_array['answer_panel']['background'];
        $answerfont_color=$css_attr_array['answer']['color'];

        $question_color=array("color"=>$questionfont_color,"background"=>$questionbg_color);
        $answer_color=array("color"=>$answerfont_color,"background"=>$answerbg_color);
        $color=array("question"=>$question_color,"answer"=>$answer_color);
        
    }
  
    
    return json_encode($color);
  }
  
  
  public function cssDefault()
  {
    $shop_name = session('shop');
    $shop_find = ShopModel::where('store_name' , $shop_name)->first();
    $id=$shop_find->id;
    $deletecss = FrontendCSS::where('shop_id', '=' , $id)->delete();

  }
}
