<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ShopModel;

class HelpController extends Controller
{
    public function index()
    {
      $shop_name = session('shop');
      $shop_find = ShopModel::where('store_name' , $shop_name)->first();
      //dd($shop_find);
      return view('help',['shop_info' => $shop_find,'active' => 'help']);
    }
}
