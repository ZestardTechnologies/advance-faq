<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ShopModel;
use App\TopicModel;
use App\QaModel;
use App\FrontendCSS;
use App\ArrowsModel;
use App\ArrowCssModel;

class FrontPage extends Controller
{
    public function index()
    {
      $css_attr_array = array();
      $data = $_POST;
      $shop_id = $data['id'];
      $shop_find = ShopModel::where('store_encrypt' , $shop_id)->first();
      $shop_name = $shop_find->store_name;
      session()->put('shop',$shop_name);

        $topics_array = array();
        $topics = TopicModel::all();
        $topic_sorted = $topics->where('topic_status', '=', 0)->sortBy('topic_order');
		//dd(TopicModel::where('topic_status', 0)->orderBy('question', 'asc')->get());
        foreach($topic_sorted as $topic) {
            $topics_array[$topic->topic_id]['questions'] = array();
            $topics_array[$topic->topic_id]['topic_name'] = $topic->topic_name;
            $topics_array[$topic->topic_id]['topic_status'] = $topic->topic_status;
            $question_unsorted = $topic->questions;
            if($shop_name == "charging-angels.myshopify.com")
			{
				$question_sorted = $question_unsorted->where('qa_status', '=', 0)->sortBy('question');
			}
			else
			{			
				$question_sorted = $question_unsorted->where('qa_status', '=', 0)->sortBy('qa_order');
			}
            foreach($question_sorted as $question)  {
              $topics_array[$topic->topic_id]['questions'][$question->qa_id] =
                array(
                  'question' => $question->question,
                  'answer' => $question->answer,
                );
            }
          }

          $style = FrontendCSS::get();
          foreach($style as $css) {
              $css_attr_array[$css->css_element][$css->css_attr] = $css->css_attr_value;
          }
          
          $arrows = ArrowCssModel::where('shop_id' , '=' , $shop_find->id)->first();
          $arrows_pattern = ArrowsModel::where('id' , '=' , $arrows->arrow_id)->first();
          return view('frontpage',['topics_array'=>$topics_array,'style'=>$css_attr_array,'arrowcss'=>$arrows,'arrow'=>$arrows_pattern]);

    }
}
