<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PublishedTrait;

class TopicModel extends Model
{
  use PublishedTrait;
  protected $table = 'topic';
  protected $primaryKey = 'topic_id';
  protected $fillable =[
    'topic_name',
    'topic_status',
    'topic_order',
    'shop_id'
  ];
  public function questions()
  {
    return $this->hasMany('App\QaModel','topic_id' , 'topic_id');
  }
  public function getShopIdColumn()
  {
    return 'shop_id';
  }
}
