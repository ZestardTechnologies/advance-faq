<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArrowsModel extends Model
{
    protected $table = 'arrows';
    protected $primaryKey = 'id';
}
