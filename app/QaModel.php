<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\PublishedTrait;

class QaModel extends Model
{
  use PublishedTrait;

  protected $table = 'question_answer';
  protected $primaryKey = 'qa_id';
  protected $fillable =[
    'question',
    'answer',
    'topic_id',
    'qa_status',
    'qa_order',
    'shop_id',
    'encrypt_id'
  ];

  public function getShopIdColumn()
  {
    return 'shop_id';
  }
}
