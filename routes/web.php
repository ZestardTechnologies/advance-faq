<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::get('dashboard', 'topicController@dashboard')->name('dashboard');

Route::get('topic','topicController@index')->name('topic');

Route::post('addtopic','topicController@store');

Route::get('addnewtopic', function () {
    return view('addtopic')->with('active', 'topic');
})->name('addnewtopic');

Route::any('edittopic/{id}','topicController@edit')->name('edittopic');

Route::post('topicupdate/{id}','topicController@update')->name('topicupdate');

Route::any('deletetopic/{id}','topicController@destroy')->name('deletetopic');

Route::get('qalist','QaController@index')->name('qalist');

Route::get('addqa','QaController@add')->name('addqa');

Route::post('addnewqa','QaController@store');

Route::get('editqa/{id}','QaController@edit')->name('editqa');

Route::post('qaupdate/{id}','QaController@update')->name('qaupdate');

Route::any('deleteqa/{id}','QaController@destroy')->name('deleteqa');

Route::get('help', 'HelpController@index')->name('help');

Route::post('faq', 'FrontPage@index')->middleware('cors')->name('faq');

Route::get('load/frontend/font','topicController@loadFont');

Route::get('load/frontend/color','topicController@loadColor');

Route::get('load/frontend/css','topicController@loadFrontendCSS');

Route::post('frontend/css' , 'topicController@frontendCss');

Route::post('post/frontend/css' , 'topicController@postFrontendCss');

Route::post('frontend/css/default','topicController@cssDefault');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::get('load/arrow', 'ArrowController@index');

Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status'); 