@yield('header')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Advance FAQs</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    
    <!-- jquery datatable CSS -->
    <link rel="stylesheet" href="{{ asset('css/datatable/dataTables.bootstrap.min.css') }}">

    <!-- Notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.css') }}">
    
    <link rel="stylesheet" href="{{asset('js/fontselector/fontselect.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('js/colorpicker/spectrum.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/accordian.css') }}" type="text/css">

    <!-- custome CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- jquery datatable script -->
    <script src="{{ asset('js/3.2.1.jquery.min.js') }}"></script>
    <!-- Latest compiled JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatable/dataTables.bootstrap.min.js') }}"></script>

    <!-- Notification -->
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <!-- Custome js -->
    <script src="{{ asset('js/javascript.js') }}"></script>

    <!-- accordion script -->
    <script src="{{ asset('js/aria-accordion.js') }}"></script>
    
    <!-- Copy to clipboard script -->
    <script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>

    <script>
      $(document).ready(function() {
          $('#topiclist').DataTable({
            "paging":   true,
            "ordering": true
          });
        });
    </script>
    <!-- shopify Cript for fast load -->
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script>
      ShopifyApp.init({
        apiKey: '3843b0bb4dce58bd34a20291a6928488',
        shopOrigin: '<?php echo "https://".session('shop'); ?>'
      });

      ShopifyApp.ready(function() {
          ShopifyApp.Bar.initialize({
            icon: '',
            title: '',
            buttons: { }
          });
        });
    </script>
    
    <style>
            .smilotrics_faq a {
                text-decoration: none;
                color: inherit;
            }

            .smilotrics_faq .description {
                margin: 1em auto 2.25em;
            }

            .smilotrics_faq ul {
                list-style: none;
                padding: 0;
            }
            .smilotrics_faq ul .answer_panel {
                padding-left: 1em;
                overflow: hidden;
                display: none;
            }
            .smilotrics_faq ul .answer_panel.show {
                /*display: block;*/
            }
            .smilotrics_faq ul li {
                margin: .5em 0;
            }
            .smilotrics_faq ul li a.question_panel {
                width: 100%;
                display: block;
                background: rgba(0, 0, 0, 0.78);
                color: #fefefe;
                padding: .75em;
                border-radius: 0.15em;
                transition: background .3s ease;
            }
            
            .smilotrics_faq ul li a.toggle:hover {
                background: rgba(0, 0, 0, 0.9);
                color: #fefefe;
            }

            @media (min-width: 768px){
                #sidebar {
                    top: 0px;
                    bottom: 0;
                    left: 0;
                    transition: width 0.3s ease;
                }
                
            }

            .page{
                margin: 0;
                padding: 0;
                height: 100%;
                display: block;
                border:solid #000 1px;
            }
            .active_panel {
                /* hide it for small displays*/
                display: none;
                padding : 0;
                margin: 0;
            }

            .navbar-default .navbar-toggle {
                border-color: transparent; /* Removes border color */
                float: left; /* Move navbar toggle to left */
            }
            .navbar-default .navbar-toggle .icon-bar {
                background-color: #f00; /* Changes regular toggle color */
            }
            .navbar-default .navbar-toggle .icon-bar:hover {
                background-color: #fff; /* Changes toggle color on hover */
            }
            
            .header{
                margin-bottom: 20px;
            }

            .navbar-default {
               margin-bottom: 0px !important;
            }

            .review-div {
                text-align: center !important;
                width: 100%;
                background-color: #fafad0;
                padding: 10px;
                box-shadow: 1px 1px 2px #777;
                display: inline-block;
                position: sticky;
                top: 0px;
                left: 0px;
                z-index: 100;
            }
        </style>
  </head>

  <body>

@yield('navigation')
<? if(!isset($active)){$active="";} ?>
             <div class="header clearfix">
                 <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <ul class="nav navbar-nav">
                      <li @if($active == 'manage')class="active" @else class="" @endif><a href="{{ route('dashboard') }}">Manage</a></li>
                      <li @if($active == 'topic')class="active" @else class="" @endif><a href="{{ route('topic') }}">Topic</a></li>
                      <li @if($active == 'qa')class="active" @else class="" @endif><a href="{{ route('qalist') }}">Q/A</a></li>
                      <li @if($active == 'help')class="active" @else class="" @endif><a href="{{ route('help') }}">Help?</a></li>
                    </ul>
                  </div>
                 </nav>
              <div class="review-div">
                Are you loving this app? Please spend 2mins to help us <a href="https://apps.shopify.com/advance-faq?reveal_new_review=true" target="_blank">write a review</a>. 
              </div>
             </div>
             
@yield('content')

<script>
  @if(Session::has('notification'))

    var type = "{{ Session::get('notification.alert-type', 'info') }}";
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;
        case 'options':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;

    }
  @endif
</script>
<!-- validation -->
<script src="{{ asset('js/validator.js') }}"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
