@extends('header')

@section('content')
<div class="container formcolor">
  <form action="{{ url('/addtopic') }}" name="topic" data-toggle="validator" role="form" method="post">
    {{ csrf_field() }}
        <div class="col-sm-6 page-left-section">
            <h3 class="ribbon">Add Topic</h3>
        </div>
        <div class="add-topic-form">
            <div class="form-group">
                <label class="control-label">Topic Name:</label>
                <input type="text" name="topic_name" class="form-control" placeholder="Topic Name" required>
                <div class="help-block with-errors">*This field is Required</div>
            </div>
            <div class="form-group">
                <label class="control-label">Topic Status:</label>
                <select name="topic_status" class="form-control">
                    <option value="0">Active</option>
                    <option value="1">Deactive</option>
                </select>
            </div>
            <div class="form-group">
	        <label class="control-label">Sort Order:</label>
	        <input type="text" name="topic_order" class="form-control" placeholder="Topic Order">
	       </div>
            <div class="form-group">
	        <input type="submit" name="add_topic" value="Add Topic" class="btn btn-primary">
                <a href="{{ url('topic') }}"><img src="{!! asset('image/undo_back.png') !!}" height="23px"/></a>
            </div>
        </div>
  </form>
</div>
@endsection
