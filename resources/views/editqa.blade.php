@extends('header')

@section('content')
<div class="container formcolor">
  <form action="{{ url('qaupdate/'.$qa->encrypt_id) }}" name="editquestion" data-toggle="validator" role="form" method="post">
    {{ csrf_field() }}
        <div class="col-sm-6 page-left-section">
            <h3 class="ribbon">Edit Question</h3>
        </div>
        <div class="qa-list-edit-form">
            <div class="form-group">
                <label class="control-label">Question:</label>
                <textarea name="question" value="{{ $qa->question }}" class="form-control" required>{{ $qa->question }}</textarea>
                <div class="help-block with-errors">*This field is Required</div>
            </div>
            <div class="form-group">
                <label class="control-label">Answer:</label>
                <textarea id="answer" name="answer" value="{{$qa->answer}}" class="form-control" required>{{$qa->answer}}</textarea>
                @ckeditor('answer')
                <div class="help-block with-errors">*This field is Required</div>
            </div>
            <div class="form-group">
                <label class="control-label">Topic:</label>
                <select name="topic" class="form-control">
                    <option value="0">Uncategorised</option>
                    @foreach($topics as $topic)
                    <option value="{{ $topic->topic_id }}" {{ $topic->topic_id == $qa->topic_id ? 'selected=selected' : ' ' }}>{{$topic->topic_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">Question Status:</label>
                    @if($qa->qa_status == 0)
                    <select name="qa_status" class="form-control">
                        <option value="0" selected="selected">Active</option>
                        <option value="1">Deactive</option>
                    </select>
                    @elseif($qa->qa_status == 1)
                    <select name="qa_status" class="form-control">
                        <option value="0">Active</option>
                        <option value="1" selected="selected">Deactive</option>
                    </select>
                    @else
                    <select name="qa_status" class="form-control">
                        <option value="0">Active</option>
                        <option value="1">Deactive</option>
                    </select>
                    @endif
        </div>
        <div class="form-group">
            <label class="control-label">Sort Order:</label>
            <input type="text" name="qa_order" value="{{ $qa->qa_order }}" class="form-control">
        </div>
        <div class="form-group">
            <input type="submit" name="edit_qa" value="Save" class="btn btn-primary">
            <a class="goback" href="{{ url('qalist') }}"><img src="{!! asset('image/undo_back.png') !!}" />Go Back</a>
        </div>
        </div>
  </form>
</div>
@endsection
