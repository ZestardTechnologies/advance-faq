@extends('header')

@section('content')
<div class="container formcolor">
  <form action="{{ url('/addnewqa') }}" name="addquastions" data-toggle="validator" role="form" method="post">
    {{ csrf_field() }}
        <div class="col-sm-6 page-left-section">
          <h3 class="ribbon">Add Question</h3>
        </div>
        <div class="qa-list-add-form">
            <div class="form-group">
                <label class="control-label">Question:</label>
                <textarea name="question" class="form-control" placeholder="Question" required></textarea>
                <div class="help-block with-errors">*This field is Required</div>
            </div>
            <div class="form-group">
                <label class="control-label">Answer:</label>
                <textarea id="answer" name="answer" class="form-control"placeholder="Answer" required></textarea>
                @ckeditor('answer')
                <div class="help-block with-errors">*This field is Required</div>
            </div>
            <div class="form-group">
                <label class="control-label">Topic:</label>
                <select name="topic" class="form-control" required>
                    <option value="">Select Topic</option>
                        @foreach($topics as $topic)
                    <option value="{{ $topic->topic_id }}">{{$topic->topic_name}}</option>
                        @endforeach
                </select>
                <div class="help-block with-errors">*This field is Required</div>
              </div>
        <div class="form-group">
		        <label class="control-label">Question Status:</label>
            <select name="qa_status" class="form-control">
                <option value="0">Active</option>
                <option value="1">Deactive</option>
            </select>
	      </div>
        <div class="form-group">
		        <label class="control-label">Sort Order:</label>
		        <input type="text" name="qa_order" class="form-control" placeholder="Q/A Order">
	      </div>

        <div class="form-group">
		        <input type="submit" name="add_qa" value="Add Questions" class="btn btn-primary">
            <a href="{{ url('qalist') }}"><img src="{!! asset('image/undo_back.png') !!}" height="23px"/></a>
	      </div>
        </div>
  </form>
</div>
@endsection
