@extends('header')

@section('content')
<div class="col-sm-7">
    <div class="content help-use-section">
        <h1 class="ribbon">How to Use?</h1>
        <div class="help-use-section">
            <div>
                1) First step, Go to <a href="topic">"Topic"</a> section.<br/>
                <ul>
                    <li>click on "Add" topic button to add the new topic like as category.</li>
                    <li>After added the new topic, you can also Edit the existing topic by clicking on the <a href="#"><span class="glyphicon glyphicon-edit"></span></a> icon.</li>
                    <li>Also Delete the existing topic by clicking on the <a href="#"><span class="glyphicon glyphicon-trash"></span></a> icon.</li>
                </ul>
            </div>
            <div>
                2) Second step, Go to <a href="qalist">"Q/A"</a> section.<br/>
                <ul>
                    <li>click on "Add" Question button to add new Question & Answer.</li>
                    <li>After added the Question & Answer, you can also Edit the existing Question & Answer by clicking on the <a href="#"><span class="glyphicon glyphicon-edit"></span></a> icon.</li>
                    <li>Also Delete the existing Question & Answer by clicking on the <a href="#"><span class="glyphicon glyphicon-trash"></span></a> icon.</li>
                </ul>
            </div>
            <?php
                $url = ''; 
                if(Session::has('shop')){
                    $url = "https://".session('shop')."/pages/advance-faq";
                }
                else{
                    $url = "#";
                }

                $pageUrl = '#';
                if(Session::has('shop')){
                    $pageUrl = "https://".session('shop')."/admin/pages";
                }
            ?>
            <div>
                3) In the Last step, Go To <a href="dashboard">"Manage"</a> section To Customise the Appearance.
                <ul>
                    <li>Here, you can set The font-style, font-color, background-colors and Arrows as well as many things.</li>
                    <li>When changing something in Manage section, you get the preview in right side panel.</i>
                    <li>After adding the Topics and Q/A, you can check it by click on <a href="<?php echo $url ?>" target="_blank">Advance FAQ page.</a></li>
                </ul>
            </div>                                    
            <div>
                <div class="form-group">
                    <p class="control-label">
                        4) If you want to add FAQ's in specific page and location So you can Copy and past the Following code in that page for go in page section click on 
                        <a href="<?php echo $pageUrl ?>" target="_blank"><b>Page Section.</b></a>
                    </p>                    
                </div>
            </div>            
            <div class="wrapper-box">
                <label class="form-control short_code" id="short_code" disabled>&lt;div class="advanced_faq" id="{{ $shop_info->store_encrypt }}"&gt;&lt;/div&gt;</label>
                <btn class="btn btn-primary" data-clipboard-target=".short_code" style="display: block;" onclick="copy_text()">Copy</btn>                
            </div>
        </div>
    </div>
    <!----  -->
    <div class="content help-use-section">
        <h1 class="ribbon">Additional Instruction</h1>
        <div class="help-use-section">
            <p>Advance FAQ app allows you to create a separate list of questions and answers related to specific topics. It enables admin to solve customer queries quickly by providing relevant FAQs.</p>

            <p>Customers may have many questions and inquiries in their mind and can search for answers on your FAQ page. If they can find direct solutions to their queries on your FAQ page it can save your crucial time and moreover they can get appropriate responses without even connecting to you.</p>

            <p>This is the main benefit of having a basic FAQ page, it saves yours as-well as your's customer's precious time.</p>

            <h3>Interesting Features</h3>

            <ul>
            <li>Create multiple Topic and Question/Answers from admin</li>
            <li>Admin can easily change the sort order of Topic and Question/Answers</li>
            <li>Admin can active/deactivate the Topic and Question/Answers</li>
            <li>All the Question/Answers show as per Topic(category wise)</li>
            <li>Admin can see the design preview from admin</li>
            <li>Admin will change the design of FAQ view as per requirement or depends on theme colors</li>
            <li>Now an Advanced FAQ page has been made for you, it can be found in the admin to go to Online Store > Pages > Advanced FAQ.</li>
            <li>Store owner adds the New Advanced FAQ page to the menu.
            click <a href="https://help.shopify.com/manual/sell-online/online-store/menus-and-links#add-a-link-to-a-menu" target="_blank">here</a> to show how to add Advanced FAQ page link to the menu.</li>
            <li>No need to manual setup</li>
            <li>Just need to Copy and Paste the shortcode to show FAQ anywhere in the store.</li>
            <li>Responsive and Mobile Friendly FAQ view</li>
            </ul>
        </div>  
    </div>
</div>
<div class="col-sm-5">
    <div class="content benefit-section">
        <h1 class="ribbon">Benefits.</h1>
        <ul class="benefits">
            <li>Easy To Manage</li>
            <li>Easy to search and filter Topic and Questions list</li>
            <li>Admin can customise the Appearance.</li>
            <li>Responsive (Mobile / Tablet Friendly) Sliders</li>
            <li>Admin can have the option to set slider style differently for each slider</li>
            <li>Slider automatically generates short code and Admin can paste it wherever it needs to be shown</li>
        </ul>
    </div>
</div>

<style>
.wrapper-box {
    width: 100%;
    display: inline-block;
}

label#short_code {
    display: inline-block;
    margin: 0;
    float: left;
    border-radius: 0;
    height: auto !important;
}

btn.btn.btn-primary {
    width: 20%;
    display: inline-block !important;
    border-radius: 0;
}
</style>
@endsection
