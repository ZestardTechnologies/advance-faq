@extends('header')

@section('content')
<section class="accordion-group" id="accordion-group-2">
  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Customize Question Design</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">

        <div id="question_design" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="option1">
          <div class="panel-body">
            <h4>Customize font:</h4>
              <input type="text" id="font" />
            <br/>
            <h4>Customize color:</h4>
              Background <input type = "text" id = "question_background"/>
              Font <input type = "text" id = "question_font"/>
            <br/>
            <br/>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Customize Answer Design</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="answer_design" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option2">
          <div class="panel-body">
            <h4>Customize font:</h4>
              <input type="text" id="font_answer" />
            <br/>
            <h4>Customize color:</h4>
              Background <input type = "text" id = "answer_background"/>
              Font <input type = "text" id = "answer_font"/>
            <br/>
            <br/>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Separator</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="separator" class="panel-collapse collapse"   role="tabpanel"  aria-labelledby="option3">
          <div class="panel-body">
            <div class="ui-field-contain">
                <label>Seperator Alignment:</label>
                <select name="select-native-2" id="select-native-2" data-mini="true">
                  <option>Left</option>
                  <option>Center</option>
                  <option>Right</option>
                </select>
            </div>
              <label>Seperator Width:</label>
              <input type="range" name="points" id="points" value="" min="0" max="100">
              <label>Seperator Height:</label>
              <input type="range" name="points" id="points" value="" min="0" max="100">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Transitions</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="transitions" class="panel-collapse collapse" role="tabpanel"  aria-labelledby="option4">
          <div class="panel-body">
            <div data-role="rangeslider">
                <label>Transition Speed:</label>
                <input type="range" name="range-10a" id="range-10a" min="0" max="1.5" step=".1" value="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Borders</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="borders" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option5">
          <div class="panel-body">
            <label>Border Width:</label>
            <input type="range" name="points" id="points" value="" min="0" max="10">
            <label>Border Radius:</label>
            <input type="range" name="points" id="points" value="" min="0" max="20">
            <label>Border Color:</label>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Shadows</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="shadows" class="panel-collapse collapse" role="tabpanel"  aria-labelledby="option6">
          <div class="panel-body">
            <div class="ui-field-contain">
                <label>Background Shadow:</label>
                <select name="select-native-2" id="select-native-2" data-mini="true">
                  <option>none</option>
                  <option>Hard Dark Shadow</option>
                  <option>Soft Dark Shadow</option>
                  <option>Hard Light Shadow</option>
                  <option>Soft Light Shadow</option>
                </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Plugin Size</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="plugin_size" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option7">
          <div class="panel-body">
            <label>Plugin Width:</label>
            <input type="range" name="points" id="points" value="" min="75" max="100">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Hide Plugin</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="hide_plugin" class="panel-collapse collapse" role="tabpanel"  aria-labelledby="option8">
          <div class="panel-body">
            <label for="flip-mini">Plugin Status:</label>
            <select name="flip-mini" id="flip-mini" data-role="slider" data-mini="true">
              <option value="off">Off</option>
              <option value="on">On</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Advanced</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="advanced" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option9">
          <div class="panel-body">
            advanced
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="accordion-group__accordion">
    <header class="accordion-group__accordion-head">
      <h3 class="accordion-group__accordion-heading"><button type="button" class="accordion-group__accordion-btn">Remove Power Logo</button></h3>
    </header>
    <div class="accordion-group__accordion-collapse">
      <div class="accordion-group__accordion-content">
        <div id="remove_power_logo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option10">
          <div class="panel-body">
            remove power logo
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
@endsection
