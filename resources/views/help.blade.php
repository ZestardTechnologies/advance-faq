@extends('header')
@section('content')
<?php $store_name = session('shop'); ?>
<link rel="stylesheet" href="{{ asset('css/design_style.css') }}" />
<div class="container formcolor formcolor_help" >
    <div class=" row">
        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">              
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <img src="" class="imagepreview" style="width: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="help_page">            
            <div class="col-md-12 col-sm-12 col-xs-12 need_help">
                <div class="success-copied"></div>
                <h2 class="dd-help">Need Help?</h2>
                <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="dd-help-ul">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Configuration Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                                        <strong><span class="">How to Use?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div>
                                        1) First step, Go to <a href="topic">"Topic"</a> section.
                                        <ul class="ul-help">
                                            <li>Click on "Add" topic button to add the new topic like as category.</li>
                                            <li>After added the new topic, you can also Edit the existing topic by clicking on the <a href="#"><span class="glyphicon glyphicon-edit"></span></a> icon.</li>
                                            <li>Also Delete the existing topic by clicking on the <a href="#"><span class="glyphicon glyphicon-trash"></span></a> icon.</li>                                        
                                        </ul> 
                                    </div>
                                    <div>
                                        2) Second step, Go to <a href="qalist">"Q/A"</a> section.<br>
                                        <ul>
                                            <li>Click on "Add" Question button to add new Question &amp; Answer.</li>
                                            <li>After added the Question &amp; Answer, you can also Edit the existing Question &amp; Answer by clicking on the <a href="#"><span class="glyphicon glyphicon-edit"></span></a> icon.</li>
                                            <li>Also Delete the existing Question &amp; Answer by clicking on the <a href="#"><span class="glyphicon glyphicon-trash"></span></a> icon.</li>
                                        </ul>
                                    </div>
                                    <?php
                                    $url = '';
                                    if (Session::has('shop')) {
                                        $url = "https://" . session('shop') . "/pages/advance-faq";
                                    } else {
                                        $url = "#";
                                    }

                                    $pageUrl = '#';
                                    if (Session::has('shop')) {
                                        $pageUrl = "https://" . session('shop') . "/admin/pages";
                                    }
                                    ?>
                                    <div>
                                        3) In the Last step, Go To <a href="dashboard">"Manage"</a> section To Customise the Appearance.
                                        <ul>
                                            <li>Here, you can set The font-style, font-color, background-colors and Arrows as well as many things.</li>
                                            <li>When changing something in Manage section, you get the preview in right side panel.</li>
                                            <li>After adding the Topics and Q/A, you can check it by click on <a href="<?php echo $url ?>" target="_blank">Advance FAQ page.</a></li>
                                        </ul>
                                    </div>
                                    <div>                                        
                                        4) If you want to add FAQ's in specific page and location So you can copy and paste the following code in that page for go in page section click on <a href="<?php echo $pageUrl ?>" target="_blank"><b>Page Section.</b></a>                                        
                                    </div>
                                    <div class="copystyle_wrapper col-md-5">
                                        <textarea rows="1" class="form-control popup_code" id="product-shortcode" disabled>&lt;div class="advanced_faq" id="{{ $shop_info->store_encrypt }}"&gt;&lt;/div&gt;</textarea>
                                        <btn id="copyproductBtn" name="copyproductBtn" value="Copy Popupcode" class="btn btn-info copycss_button copyMe" data-clipboard-target="#product-shortcode" style="display: block;" onclick="copyToClipboard('#product-shortcode')"><i class="fa fa-check"></i> Copy</btn>
                                    </div>
                                </div>
                            </div>
                        </div>                       

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                                        <strong><span class="">Additional Instruction</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Advance FAQ app allows you to create a separate list of questions and answers related to specific topics. It enables admin to solve customer queries quickly by providing relevant FAQs.</p>
                                    <p>Customers may have many questions and inquiries in their mind and can search for answers on your FAQ page. If they can find direct solutions to their queries on your FAQ page it can save your crucial time and moreover they can get appropriate responses without even connecting to you.</p>
                                    <p>This is the main benefit of having a basic FAQ page, it saves yours as-well as your's customer's precious time.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                                        <strong><span class="">Interesting Features</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>Create multiple Topic and Question/Answers from admin</li>
                                        <li>Admin can easily change the sort order of Topic and Question/Answers</li>
                                        <li>Admin can active/deactivate the Topic and Question/Answers</li>
                                        <li>All the Question/Answers show as per Topic(category wise)</li>
                                        <li>Admin can see the design preview from admin</li>
                                        <li>Admin will change the design of FAQ view as per requirement or depends on theme colors</li>
                                        <li>Now an Advanced FAQ page has been made for you, it can be found in the admin to go to Online Store > Pages > Advanced FAQ.</li>
                                        <li>Store owner adds the New Advanced FAQ page to the menu.
                                            click <a href="https://help.shopify.com/manual/sell-online/online-store/menus-and-links#add-a-link-to-a-menu" target="_blank">here</a> to show how to add Advanced FAQ page link to the menu.</li>
                                        <li>No need to manual setup.</li>
                                        <li>Just need to Copy and Paste the shortcode to show FAQ anywhere in the store.</li>
                                        <li>Responsive and Mobile Friendly FAQ view</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                                        <strong><span class="">Benefits</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>Easy To Manage</li>
                                        <li>Easy to search and filter Topic and Questions list</li>
                                        <li>Admin can customise the Appearance.</li>
                                        <li>Responsive (Mobile / Tablet Friendly) Sliders</li>
                                        <li>Admin can have the option to set slider style differently for each slider</li>
                                        <li>Slider automatically generates short code and Admin can paste it wherever it needs to be shown</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--Uninstall Process Div start-->
<!--            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Uninstall Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">                        
                        <ul class="ul-help">
                            <li>To uninstall the app, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                            <li>Click on delete icon of Related Products and Blogs.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/uninstallApp.png') }}"><b> See Example</b></a></li>
                            <li>If possible remove the shortcode from <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a> and <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/article-template.liquid" target="_blank"><b>article-template.liquid</b></a> files.</li>
                        </ul>                        
                    </div>
                </div>
            </div>-->
            <!--Uninstall Process Div end-->
        </div>
        <!--Version updates-->
        <!--        <div class="version_update_section">
                    <div class="col-md-6" style="padding-right: 0;">
                        <div class="feature_box">
                            <h3 class="dd-help">Version Updates <span class="verison_no">2.0</span></h3>
                            <div class="version_block">
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-heart"></i></p>
                                        <strong>22 Jan, 2018</strong>
                                        <a href="#"><b>Update</b></a>
                                    </div>
                                    <div class="col-md-8 version_details">
                                        <strong>Version 2.0</strong>
                                        <ul>
                                            <li>Add Delivery Date & Time information under customer order Email</li>
                                            <li>Auto Select for Next Available Delivery Date</li>
                                            <li>Auto Tag Delivery Details to all the Orders</li>
                                            <li>Manage Cut Off Time for Each Individual Weekday</li>
                                            <li>Limit Number of Order Delivery during the given time slot for any day </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-globe"></i></p>
                                        <strong>20 Dec, 2017</strong>
                                        <a href="#"><b>Release</b></a>
                                    </div>
                                    <div class="col-md-8 version_details version_details_2">
                                        <strong>Version 1.0</strong>
                                        <ul>
                                            <li>Delivery Date & Time Selection</li>
                                            <li>Same Day & Next Day Delivery</li>
                                            <li>Blocking Specific Days and Dates</li>
                                            <li>Admin Order Manage & Export Based on Delivery Date</li>
                                            <li>Option for Cut Off Time & Delivery Time</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="feature_box">
                            <h3 class="dd-help">Upcoming Features</h3>
                            <div class="feature_block">
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox0" name="exclude_block_date_status">
                                        <label for="checkbox0"></label>
                                    </span> 
                                    <strong>Multiple Cutoff Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox4" name="exclude_block_date_status">
                                        <label for="checkbox4"></label>
                                    </span> 
                                    <strong>Multiple Delivery Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">
                                        <label for="checkbox5"></label>
                                    </span> 
                                    <strong>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox1" name="exclude_block_date_status">
                                        <label for="checkbox1"></label>
                                    </span> 
                                    <strong>Auto Select for Next Available Delivery Date</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox2" name="exclude_block_date_status">
                                        <label for="checkbox2"></label>
                                    </span> 
                                    <strong>Order Export in Excel</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox3" name="exclude_block_date_status">
                                        <label for="checkbox3"></label>
                                    </span> 
                                    <strong>Filtering Orders by Delivery Date</strong>  
                                </div>                                        
                            </div>
                            <div>
                                <p class="feature_text">
                                    New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>-->

    </div>
</div>
<script>
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>
<script>
    var el1 = document.getElementById('copyproductBtn');
    var el2 = document.getElementById('copyblogBtn');
    var el3 = document.getElementById('copyrandomproductBtn');
    if (el1) {
        el1.addEventListener("click", function () {
            copyToClipboard(document.getElementById("product-shortcode"));
        });
    }
    if (el2) {
        el2.addEventListener("click", function () {
            copyToClipboard(document.getElementById("blog-shortcode"));
        });
    }
    if (el3) {
        el3.addEventListener("click", function () {
            copyToClipboard(document.getElementById("randomproduct-shortcode"));
        });
    }
    /*document.getElementById("copyproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("product-shortcode"));
     });
     document.getElementById("copyblogBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("blog-shortcode"));
     });
     document.getElementById("copyrandomproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("randomproduct-shortcode"));
     });*/


    function copyToClipboard(elem) {
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }
        if (isInput) {
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            target.textContent = "";
        }
        return succeed;
    }
</script>
@endsection
