<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<?php
if(!empty($style))
{
    $question=$style['question'];
    $answer=$style['answer'];
    $question_panel=$style['question_panel'];
    $answer_panel=$style['answer_panel'];
    $font=$question['font-family'];
    $font_answer=$answer['font-family'];
?>
    <style>
    .advanced_faq a {
      text-decoration: none;
      color: inherit;
    }

     .advanced_faq .description {
      margin: 1em auto 2.25em;
    }

    .advanced_faq ul {
      list-style: none;
      padding: 0;
    }
    .advanced_faq ul .answer_panel {
      margin: 0px;
      padding-left: 1em;
      overflow: hidden;
      display: none;
      <?php 
      foreach($answer as $answeratribute=>$answervalue)
      {
          echo $answeratribute.":".$answervalue.";";
      }
      
      foreach($answer_panel as $answerpanel_atribute=>$answerpanel_value)
      { 
          echo $answerpanel_atribute.":".$answerpanel_value.";";     
      }
      ?>
    }
    .advanced_faq ul .answer_panel.show {
      display: block;
    }
    .advanced_faq ul li {
      margin: .5em 0;
    }
    .advanced_faq ul li a.question_panel {
      width: 100%;
      display: block;
      padding: .75em;
      border-radius: 0.15em;
      transition: background .3s ease;
      <?php
      foreach($question as $atribute=>$value)
      { 
          echo $atribute.":".$value.";"; 
      }      
      foreach($question_panel as $panel_atribute=>$panel_value)
      { 
          echo $panel_atribute.":".$panel_value.";";     
      }
      ?>
    }
    .advanced_faq ul li a.toggle:hover {
      background: rgba(0, 0, 0, 0.9);
      color: #fefefe;
    }
    /*for accordian with i*/
    .arrows-default{
        display: none;
    }
    .arrows-on{
        display: block;
        margin-left: 5px;
        margin-right: 5px;
    }
    .advanced_faq .glyphicon {
        line-height: 1.5; 
    }
    </style>
<?php
  }
  else{
?>
  <style>
        .advanced_faq a {
          text-decoration: none;
          color: inherit;
        }

         .advanced_faq .description {
          margin: 1em auto 2.25em;
        }

        .advanced_faq ul {
          list-style: none;
          padding: 0;
        }
        .advanced_faq ul .answer_panel {
          padding-left: 1em;
          overflow: hidden;
          display: none;
          transition: background 3s ease;
        }
        .advanced_faq ul .answer_panel.show {
          margin: 0px;
          display: block;
          transition: background 3s ease;
        }
        .advanced_faq ul li {
          margin: .5em 0;
        }
        .advanced_faq ul li a.question_panel {
          width: 100%;
          display: block;
          background: rgba(0, 0, 0, 0.78);
          color: #fefefe;
          padding: .75em;
          border-radius: 0.15em;
          transition: background 3s ease;
        }
        .advanced_faq ul li a.toggle:hover {
          background: rgba(0, 0, 0, 0.9);
          color: #fefefe;
        }
        
        /*for accordian with i*/
        .arrows-default{
            display: none;
        }
        .arrows-on{
            display: block;
            margin-left: 5px;
            margin-right: 5px;
        }
        .advanced_faq .glyphicon {
            line-height: 1.5; 
        }
        
  </style>
<?php
 }
 ?>
@foreach($topics_array as $topic_id=>$topic)
          <h3>{{ $topic['topic_name'] }}</h3>
          @foreach($topic['questions'] as $question)
          <ul class="accordion">
            <li>
              <a class="question_panel" href="javascript:void(0);">
                <span class="question">
              		{{ $question['question'] }}
              	</span>
                <i class="glyphicon {{ $arrow->up }} arrow arrow-up arrows-default " arrowup_tag="{{ $arrow->up }}" @if(count($arrowcss)>0)style="float: {{ $arrowcss->arrow_position }}; color: {{ $arrowcss->arrow_color }};" @endif ></i>
                <i class="glyphicon {{ $arrow->down }} arrow arrow-down arrows-default arrows-on" arrowdown_tag="{{ $arrow->down }}" @if(count($arrowcss)>0)style="float: {{ $arrowcss->arrow_position }}; color: {{ $arrowcss->arrow_color }};" @endif ></i>
              </a>
              <ul class="answer_panel">
                <li class = "answer">
                	{!! $question['answer'] !!}
                </li>
              </ul>
            </li>
          </ul>
          @endforeach
@endforeach
    </select>
  <br/>
  <br/>
<script>
$('.question_panel').click(function(e) {
    e.preventDefault();

    var $this = $(this);
    $this.next().slideToggle("slow");
});

//accordian arrow
$('.question_panel').on('click', function () {
    $(this).children('.arrow-up').toggleClass('arrows-on');
    $(this).children('.arrow-down').toggleClass('arrows-on');
});
</script>
