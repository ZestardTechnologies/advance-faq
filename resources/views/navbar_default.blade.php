<div class = "header clearfix">
<nav class="navbar navbar-default" role = "navigation">
       <div id="navbar" class="collapse navbar-collapse">
         <ul class="nav navbar-nav">
           <li @if($active == 'manage')class="active" @endif><a href="{{ route('dashboard') }}">Manage</a></li>
           <li @if($active == 'topic')class="active"@endif><a href="{{ route('topic') }}">Topic</a></li>
           <li @if($active == 'qa')class="active"@endif><a href="{{ route('qalist') }}">Q/A</a></li>
            <li @if($active == 'help')class="active" @else class="" @endif><a href="{{ route('help') }}">Help?</a></li>
         </ul>
       </div>
</nav>
</div>
