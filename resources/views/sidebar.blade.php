<div class = "panel-group custom-section" role = "tablist" aria-multiselectable = "true">
  <div class="panel panel-primary">
      <div class="panel-heading zero-padding active" role="tab" id="option1">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" href="#question_design" aria-expanded="true" aria-controls="question_design" class="accordion-toggle" >
          Customize Question Design
        </a>
      </h4>
    </div>
    <div id="question_design" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="option1">
      <div class="panel-body">
        <h4>Customize font</h4>
          <div id="fontSelect" class="fontSelect">
			<div class="arrow-down"></div>
          </div>
        <div class="Questionsfont_size">
        <span>Font Size:</span> <select id="question_fontsize" class="form-control question_fontsize">
                        <option value="8"@if($font_size['question'] == '8px') selected @endif>8px</option>
                        <option value="9"@if($font_size['question'] == '9px') selected @endif>9px</option>
                        <option value="10"@if($font_size['question'] == '10px') selected @endif>10px</option>
                        <option value="11"@if($font_size['question'] == '11px') selected @endif>11px</option>
                        <option value="12"@if($font_size['question'] == '12px') selected @endif>12px</option>
                        <option value="14"@if($font_size['question'] == '14px') selected @endif>14px</option>
                        <option value="16"@if($font_size['question'] == '16px') selected @endif>16px</option>
                        <option value="18"@if($font_size['question'] == '18px') selected @endif>18px</option>
                        <option value="20"@if($font_size['question'] == '20px') selected @endif>20px</option>
                        <option value="24"@if($font_size['question'] == '24px') selected @endif>24px</option>
                        <option value="28"@if($font_size['question'] == '28px') selected @endif>28px</option>
                        <option value="36"@if($font_size['question'] == '36px') selected @endif>36px</option>
                        <option value="48"@if($font_size['question'] == '48px') selected @endif>48px</option>
                   </select>
        </div><br/>
        <hr>
        <h4>Customize color</h4>
          Background <input type = "text" id = "question_background"/>
          Font <input type = "text" id = "question_font"/>
        <br/>
        <hr>
        <h4>Padding:</h4>
            <div class = "row">
                <div class = "col-sm-8">
                    <input type="range" name="queastion-padding" id="queastion-padding" value="{{ filter_var($padding['question_panel'],FILTER_SANITIZE_NUMBER_INT) }}" min="0" max="20" oninput="showQuestionPadding(this.value)" onchange="showQuestionPadding(this.value)">
                </div>
                <div class = "col-sm-4">
                    <div class = "input-group">
                        <input type = "text" class = "form-control" id = "question-padding-text" value = "{{ filter_var($padding['question_panel'],FILTER_SANITIZE_NUMBER_INT) }}" disabled="disabled"/>
                      <span class = "input-group-addon">px</span>
                    </div>
                </div>
            </div>
        <br/>
      </div>
    </div>
    </div>
  <div class="panel panel-primary">
    <div class="panel-heading zero-padding" role="tab" id="option2">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" href="#answer_design" aria-expanded="true" aria-controls="answer_design" class="accordion-toggle">
          Customize Answer Design
        </a>
      </h4>
    </div>

    <div id="answer_design" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option2">
      <div class="panel-body">
        <h4>Customize font</h4>
          <div id="fontSelect2" class="fontSelect">
			<div class="arrow-down"></div>
          </div>
          <div class="Answerfont_size">
            <span>Font Size</span> <select id="answer_fontsize" class="form-control answer_fontsize">
                        <option value=""></option>
                        <option value="8"@if($font_size['answer'] == '8px') selected @endif>8px</option>
                        <option value="9"@if($font_size['answer'] == '9px') selected @endif>9px</option>
                        <option value="10"@if($font_size['answer'] == '10px') selected @endif>10px</option>
                        <option value="11"@if($font_size['answer'] == '11px') selected @endif>11px</option>
                        <option value="12"@if($font_size['answer'] == '12px') selected @endif>12px</option>
                        <option value="14"@if($font_size['answer'] == '14px') selected @endif>14px</option>
                        <option value="16"@if($font_size['answer'] == '16px') selected @endif>16px</option>
                        <option value="18"@if($font_size['answer'] == '18px') selected @endif>18px</option>
                        <option value="20"@if($font_size['answer'] == '20px') selected @endif>20px</option>
                        <option value="24"@if($font_size['answer'] == '24px') selected @endif>24px</option>
                        <option value="28"@if($font_size['answer'] == '28px') selected @endif>28px</option>
                        <option value="36"@if($font_size['answer'] == '36px') selected @endif>36px</option>
                        <option value="48"@if($font_size['answer'] == '48px') selected @endif>48px</option>
                   </select>
          </div>
        <hr>
        <h4>Customize color</h4>
          Background <input type = "text" id = "answer_background"/>
          Font <input type = "text" id = "answer_font"/>
        <br/>
        <hr>
        <h4>Padding</h4>
            <div class = "row">
                <div class = "col-sm-8">
                  <input type="range" name="answer-padding" id="answer-padding" value="{{ filter_var($padding['answer_panel'],FILTER_SANITIZE_NUMBER_INT) }}" min="0" max="20" oninput="showAnswerPadding(this.value)" onchange="showAnswerPadding(this.value)">
                </div>
                <div class = "col-sm-4">
                    <div class = "input-group">
                      <input type = "text" class = "form-control" id = "answer-padding-text" value = "{{ filter_var($padding['answer_panel'],FILTER_SANITIZE_NUMBER_INT) }}" disabled="disabled"/>
                      <span class = "input-group-addon">px</span>
                    </div>
                </div>
            </div>
        <br/>
      </div>
    </div>
  </div>

  <div class="panel panel-primary">
    <div class="panel-heading zero-padding" role="tab" id="option3">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" href="#borders" aria-expanded="true"  aria-controls="borders" class="accordion-toggle">
          Borders
        </a>
      </h4>
    </div>

    <div id="borders" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option3">
      <div class="panel-body">
          <h4>Border Width</h4>
          <div class = "row">
          <div class = "col-sm-8">
              <input type="range" name="border-width" id="border-width" value="{{ filter_var($border_color['width'],FILTER_SANITIZE_NUMBER_INT)}}" min="0" max="10" oninput="showBorderWidth(this.value)" onchange="showBorderWidth(this.value)">
          </div>
          <div class = "col-sm-4">
            <div class = "input-group">
          <input type = "text" class = "form-control" id = "border-width-text" value="{{ filter_var($border_color['width'],FILTER_SANITIZE_NUMBER_INT)}}" disabled="disabled"/>
          <span class = "input-group-addon">px</span>
        </div>
        </div>
        </div>
        <hr>
          <h4>Border Radius</h4>
          <div class = "row">
            <div class = "col-sm-8">
              <input type="range"  name="border-radius" id="border-radius" value="{{ filter_var($border_color['radius'],FILTER_SANITIZE_NUMBER_INT)}}" min="0" max="20" oninput="showBorderRadius(this.value)" onchange="showBorderRadius(this.value)">
            </div>
            <div class = "col-sm-4">
              <div class = "input-group">
                <input type = "text" class = "form-control" id = "border-radius-text" value="{{ filter_var($border_color['radius'],FILTER_SANITIZE_NUMBER_INT)}}" disabled="disabled"/>
                <span class="input-group-addon">px</span>
              </div>
        </div>
        </div>
        <hr>
        <h4>Border Color</h4>
        <div class = "row">
          <div class = "col-sm-12">
            <input type = "text" id = "border-color"/>
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="panel panel-primary">
    <div class="panel-heading zero-padding" role="tab" id="option4">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" href="#arrows" aria-expanded="true"  aria-controls="borders" class="accordion-toggle">
          Arrows
        </a>
      </h4>
    </div>

    <div id="arrows" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option4">
      <div class="panel-body">
          <h4>Arrow Pattern</h4>
          <div class = "row arrow-padding-bottom">
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="1" @if($arrow_css->arrow_id == "1") checked=checked @endif /> <i class="glyphicon glyphicon-chevron-up"></i><i class="glyphicon glyphicon-chevron-down"></i>
              </div>
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="2" @if($arrow_css->arrow_id == "2") checked=checked @endif /> <i class="glyphicon glyphicon-plus-sign"></i><i class="glyphicon glyphicon-minus-sign"></i>
              </div>
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="3" @if($arrow_css->arrow_id == "3") checked=checked @endif /> <i class="glyphicon glyphicon-arrow-up"></i><i class="glyphicon glyphicon-arrow-down"></i>
              </div>
          </div>
          <div class="row arrow-padding-bottom">
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="4" @if($arrow_css->arrow_id == "4") checked=checked @endif /> <i class="glyphicon glyphicon-upload"></i><i class="glyphicon glyphicon-download"></i>
              </div>
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="5" @if($arrow_css->arrow_id == "5") checked=checked @endif /> <i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-minus"></i>
              </div>
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="6" @if($arrow_css->arrow_id == "6") checked=checked @endif /> <i class="glyphicon glyphicon-menu-up"></i><i class="glyphicon glyphicon-menu-down"></i>
              </div>
          </div>
          <div class="row arrow-padding-bottom">
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="7" @if($arrow_css->arrow_id == "7") checked=checked @endif /> <i class="glyphicon glyphicon-triangle-top"></i><i class="glyphicon glyphicon-triangle-bottom"></i>
              </div>
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="8" @if($arrow_css->arrow_id == "8") checked=checked @endif /> <i class="glyphicon glyphicon-collapse-up"></i><i class="glyphicon glyphicon-collapse-down"></i>
              </div>
              <div class="col-sm-4">
                  <input type="radio" name="arrow_pattern" value="9" @if($arrow_css->arrow_id == "9") checked=checked @endif /> <i class="glyphicon glyphicon-circle-arrow-up"></i><i class="glyphicon glyphicon-circle-arrow-down"></i>
              </div>
          </div>       
        <hr>
          <h4>Arrow Color</h4>
          <div class = "row">
            <div class = "col-sm-12">
               <input type = "text" id = "arrow-color"/>
            </div>
          </div>
        <hr>
        <h4>Arrow Position</h4>
          <div class = "row arrowposition_select">
              <select name="arrow_position" id="arrow_position" class="form-control">
                  <option value="left"@if($arrow_css->arrow_position == "left") selected @endif>Left</option>
                  <option value="right"@if($arrow_css->arrow_position == "right") selected @endif>Right</option>
              </select>
          </div>
      </div>
    </div>
  </div>
</div>
