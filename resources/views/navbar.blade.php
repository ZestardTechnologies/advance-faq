<div class="header clearfix">
  <nav class="navbar navbar-default">
     <div class="container-fluid">
       <ul class="nav navbar-nav">
         <li @if($active == 'manage')class="active"@endif><a href="{{ route('dashboard') }}">Manage</a></li>
         <li @if($active == 'topic')class="active"@endif><a href="{{ route('topic') }}">Topic</a></li>
         <li @if($active == 'design')class="active"@endif><a href="{{ route('design') }}">Design</a></li>
         <li ><a href="{{ route('qalist') }}">Q/A</a></li>
       </ul>
     </div>
 </nav>
</div>
