@extends('header')
@section('content')
<div class="container formcolor">
    <div class="row">
        <div class="container">
        <div class="col-sm-6 page-left-section">
            <h3 class="page-heading ribbon">Topic List</h3>
        </div>
    <div class="col-sm-6 add-right-button">
        <a href="{{ url('addnewtopic') }}" class="btn btn-primary">Add topic</a>
    </div>
  <table id="topiclist" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
                <th style="width:10px !important;">Id</th>
                <th>Name</th>
                <th style="width:10px !important;" class="status-shorting">Status</th>
                <th style="width:10px !important;">Order</th>
                <th style="width:65px !important;" class="action-shorting">Action</th>
            </tr>
          </thead>
          <tbody>
              <?php $i = 1; ?>
            @foreach($topics as $topic)
              <tr>
                  <td>{{ $i }}</td>
                  <td>{{ $topic->topic_name }}</td>
                  <td>@if($topic->topic_status == 0)<?php echo"Active";?>@elseif($topic->topic_status == 1)<?php echo"Deactive";?>@endif</td>
                  <td>{{ $topic->topic_order }}</td>
                  <td>
                      <a href="{{ url('edittopic/'.$topic->encrypt_id) }}"><span class="glyphicon glyphicon-edit"></span></a>|
                      <a href="{{ url('deletetopic/'.$topic->encrypt_id) }}" onclick="return confirm('Are you sure you want to delete this Topic?');"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
              </tr>
              <?php $i++; ?>
            @endforeach
          </tbody>
      </table>
    </div>
    </div>
</div>
@endsection
