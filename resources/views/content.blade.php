<h4>Select Category</h4>
    <select id = "category" class = "form-control">
      @foreach($topics_array as $topic_id=>$topic)
        <option value = "{{ $topic_id }}">
          {{ $topic['topic_name'] }}
        </option>
      @endforeach
    </select>
  <br/>
  <br/>

  @foreach($topics_array as $topic_id=>$topic)
    <div class="{{ $topic_id }} questions">
      @include('accordion_child',['questions_array'=>$topic['questions']])
    </div>
  @endforeach
