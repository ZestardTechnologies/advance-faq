@extends('header')

@section('content')
<div class="container formcolor">
  <form action="{{ url('topicupdate/'.$topic->topic_id) }}" name="topic" data-toggle="validator" role="form" method="post">
    {{ csrf_field() }}
        <div class="col-sm-6 page-left-section">
            <h3 class="ribbon">Edit Topic</h3>
        </div>
        <div class="edit-topic-form">
            <div class="form-group">
                <label class="control-label">Topic Name:</label>
                <input type="text" name="topic_name" value="{{ $topic->topic_name }}" class="form-control" required>
                <div class="help-block with-errors">*This field is Required</div>
            </div>
            <div class="form-group">
                <label class="control-label">Topic Status:</label>
                @if($topic->topic_status == 0)
                <select name="topic_status" class="form-control">
                    <option value="0" selected="selected">Active</option>
                    <option value="1">Deactive</option>
                </select>
                @elseif($topic->topic_status == 1)
                <select name="topic_status" class="form-control">
                    <option value="0">Active</option>
                    <option value="1" selected="selected">Deactive</option>
                </select>
                @else
                <select name="topic_status" class="form-control">
                    <option value="0">Active</option>
                    <option value="1">Deactive</option>
                </select>
                @endif
            </div>
            <div class="form-group">
                <label class="control-label">Sort Order:</label>
                <input type="text" name="topic_order" value="{{ $topic->topic_order }}" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" name="edit_topic" value="Edit Topic" class="btn btn-primary">
                <a class="goback" href="{{ url('topic') }}" ><img src="{!! asset('image/undo_back.png') !!}" />Go Back</a>
            </div>
        </div>
  </form>
</div>
@endsection
