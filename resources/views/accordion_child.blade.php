@if(count($questions_array) > 0)
<div class="panel-group accordion" role="tablist" aria-multiselectable="true">
      <?php $i=0; ?>
  @foreach($questions_array as $id=>$question)
  <div class = "panel panel-default">
        <div class="panel-heading question-panel" role="tab" id="headingOne">
          <h4 class="panel-title question">
            <a role="button" data-toggle="collapse" href="#{{ $id }}" aria-expanded="true" aria-controls="{{ $id }}">
              {{ $question['question'] }}
              </a>
          </h4>
        </div>
          <div id="{{ $id }}" class="panel-collapse collapse <?php if($i==0) echo 'in'; ?> answer-panel" role="tabpanel" aria-labelledby="{{ $id }}">
            <div class="panel-body answer">
              {{ $question['answer'] }}
            </div>
          </div>
        <?php
          $i++;
         ?>
         </div>
  @endforeach
</div>
@else
<div class = "alert alert-warning">
  {{ "There are no questions for this topic!" }}
</div>
<br/>
<a type = "button" class = "btn btn-primary" href = "{{ url('addqa') }}">Add new question</a>
@endif
