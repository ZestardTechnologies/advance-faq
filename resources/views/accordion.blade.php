@extends('header')
@section('content')
<div class = "container">
	<div class = "row">
		<div class = "col-sm-6">
			<div id = "sidebar">
				@include('sidebar')
				<br/>
				{{ csrf_field() }}
				<div class="Configuration_buttons">
					<button class = "form-control btn btn-primary save">
						Save
					</button>
					<button class = "form-control btn btn-primary default">
						Default
					</button>
				</div>
				<br/>
				<br/>
			</div>
		</div>
		@if(count($topics_array) > 0)
		<div class = "col-sm-6 smilotrics_faq">
			<div class = "panel panel-default">
				<div class = "panel-heading preview">
					<h3 class="panel-title">Preview</h3>
				</div>
				<div class = "panel-body">
					@foreach($topics_array as $topic_id=>$topic)
					<h3>{{ $topic['topic_name'] }}</h3>
					@foreach($topic['questions'] as $question)
					<ul class="accordion">
						<li>
							<a class="question_panel" href="javascript:void(0);">
								<span class = "question">
									{{ $question['question'] }}
								</span>
								<i class="glyphicon {{ $arrow->up }} arrow arrow-up arrows-default" arrowup_tag="{{ $arrow->up }}" @if(count($arrow_css)>0)style="float: {{ $arrow_css->arrow_position }}; color: {{ $arrow_css->arrow_color }};" @endif ></i>
								<i class="glyphicon {{ $arrow->down }} arrow arrow-down arrows-default arrows-on" arrowdown_tag="{{ $arrow->down }}" @if(count($arrow_css)>0)style="float: {{ $arrow_css->arrow_position }}; color: {{ $arrow_css->arrow_color }};" @endif ></i>
							</a>
							<ul class="answer_panel">
								<li class = "answer">
									{!! $question['answer'] !!}
								</li>
							</ul>
						</li>
					</ul>
					@endforeach
					@endforeach
				</div>
			</div>
		</div>
		@else
		<div class = "col-sm-6 smilotrics_faq"> 
			<div class = "panel panel-default">
				<div class = "panel-heading">
					<h3 class="panel-title">Preview</h3>
				</div>
				<div class = "panel-body">
					<p>No any FAQ Found !</p>
					<p>Please create <a href="{{ url('/topic') }}"><b>Topic</b></a> and <a href="{{ url('/qalist') }}"><b>Q/A</b></a> To Customize The View</p>
				</div>
			</div>                    
		</div>
		@endif
	</div>
</div>
<div class="modal fade" id="new_note">
	<div class="modal-dialog">          
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Note</b></h4>
			</div>
			<div class="modal-body">
				<p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store, before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
			</div>        
			<div class="modal-footer">			
				<div class="datepicker_validate" id="modal_div">
					<div>
						<strong>Show me this again</strong>
							<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
							<label for="dont_show_again"></label></span>
					</div>      
				</div>      
			</div>      
		</div>
	</div>
</div>
<style>
    .smilotrics_faq .answer ul {
        list-style-type: square !important;
        list-style: square;
        padding-left: 15px;
    }
    .smilotrics_faq .answer ol {
        list-style-type: decimal !important;
        list-style: decimal;
        padding-left: 15px;
    }
</style>
<script type="text/javascript">
	$("#dont_show_again").change(function(){
		var checked   = $(this).prop("checked");
		var shop_name = "{{ session('shop') }}";			
		if(!checked)
		{
			$.ajax({
				url:'update-modal-status',
				data:{shop_name:shop_name},
				async:false,					
				success:function(result)
				{
					
				}
			});				
			$('#new_note').modal('toggle');
		}
	});
	if("{{ $new_install }}")
	{
		var new_install = "{{ $new_install }}";	
		if(new_install == "Y")
		{
			$('#new_note').modal('show');
		}
	}		
</script>	
<link rel="stylesheet" href="{{ asset('css/fontselector.css') }}">
<script src="https://cdn.shopify.com/s/assets/external/app.js"></script>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/fontselector/jquery.fontselect.js') }}"></script>
<script src="{{ asset('js/jquery.fontselector.js') }}"></script>
<script src="{{ asset('js/colorpicker/spectrum.js') }}"></script>
<script src = "{{ asset('js/aria-accordion.js') }}"></script>
<!-- Custome js -->
<script src="{{ asset('js/javascript.js') }}"></script>


<script type = "text/javascript">
function showBorderWidth(value) {
    var borderWidth = value;
    $("#border-width-text").attr('value', borderWidth);
    $(".question_panel").css('border-color', 'gray');
    $('.question_panel').css('border-style', 'solid');
    $('.question_panel').css('border-width', borderWidth + 'px');
}

function showBorderRadius(value) {
    var borderRadius = value;
    $("#border-radius-text").attr('value', borderRadius);
    $('.question_panel').css('border-radius', borderRadius + 'px');
}

function showBordercolor(value) {
    var borderColor = value;
    $("#border-color-text").attr('value', borderColor);
    $('.question_panel').css('border-color', borderColor);
}

function showQuestionPadding(value) {
    
    var Queastionpadding = value;
    $("#question-padding-text").val(Queastionpadding);
    $('.question_panel').css('padding', Queastionpadding + 'px');
}

function showAnswerPadding(value) {
    var Answerpadding = value;
    $("#answer-padding-text").attr('value', Answerpadding);
    $('.answer_panel').css('padding', Answerpadding + 'px');
}


//ajax loader
function ajaxindicatorstart(text)
{
    if (jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
        jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="{{ asset('image/loader.gif') }}"><div>' + text + '</div></div><div class="bg"></div></div>');
    }

    jQuery('#resultLoading').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    jQuery('#resultLoading .bg').css({
        'background': '#000000',
        'opacity': '0.7',
        'width': '100%',
        'height': '100%',
        'position': 'absolute',
        'top': '0'
    });

    jQuery('#resultLoading>div:first').css({
        'width': '250px',
        'height': '75px',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

function callAjax()
{
    jQuery.ajax({
        type: "GET",
        url: "fetch_data.php",
        cache: false,
        success: function (res) {
            jQuery('#ajaxcontent').html(res);
        }
    });
}

jQuery(document).ajaxStart(function () {
    //show ajax indicator
    ajaxindicatorstart('Please wait..');
}).ajaxStop(function () {
    //hide ajax indicator
    ajaxindicatorstop();
});

$(document).ready(function () {
    //global variable for font
    var question_font = '';
    var font_answer = '';

    //global variable for color
    var questionfont_color = '';
    var answerfont_color = '';

    //global variable background color
    var questionbg_color = '';
    var answerbg_color = '';
    
    // ajax call to set the font from the database
    $.ajax({
        type: "GET",
        url: "{{ url('load/frontend/font') }}",
        success: function (font) {
            fonts = JSON.parse(font);
            question_font = fonts['question'];
            font_answer = fonts['answer'];
            
        },
    });

    // ajax call to set the color from the database
    $.ajax({
        type: "GET",
        url: "{{ url('load/frontend/color') }}",
        success: function (color) {
            colors = JSON.parse(color);
            var question_fontcolor = colors['question'];
            questionfont_color = question_fontcolor['color'];

            var answer_fontcolor = colors['answer'];
            answerfont_color = answer_fontcolor['color'];

            var question_bgcolor = colors['question'];
            questionbg_color = question_bgcolor['background'];

            var answer_bgcolor = colors['answer'];
            answerbg_color = answer_bgcolor['background'];
        },
    });
    
    var frontendCss = {
        question: {},
		border_panel: {},
        answer: {},
        question_panel: {},
        answer_panel: {},
    };
    var ArrowCss = {
        "arrow_id":"",
        "arrow_color":"",
        "arrow_position":""
    };
    
    setTimeout(function () {
        $('#fontSelect').fontSelector({
            'hide_fallbacks': true,
            'initial': question_font,
            'selected': function (style) {
                var question_final = style.replace(/\"/g, "");
                frontendCss.question['font-family'] = question_final;
                $('.question').css('font-family', style);

            },
            'fonts': [
                'Arial,Arial,Helvetica,sans-serif',
				'Arial Black,Arial Black,Gadget,sans-serif',
				'Comic Sans MS,Comic Sans MS,cursive',
				'Courier New,Courier New,Courier,monospace',
				'Georgia,Georgia,serif',
				'Impact,Charcoal,sans-serif',
				'Lucida Console,Monaco,monospace',
				'Lucida Sans Unicode,Lucida Grande,sans-serif',
				'Palatino Linotype,Book Antiqua,Palatino,serif',
				'Tahoma,Geneva,sans-serif',
				'Times New Roman,Times,serif',
				'Trebuchet MS,Helvetica,sans-serif',
				'Verdana,Geneva,sans-serif',
				'Gill Sans,Geneva,sans-serif'
            ]
        });

        $('#fontSelect2').fontSelector({
            'hide_fallbacks': true,
            'initial': font_answer,
            'selected': function (style) {
                var answer_final = style.replace(/\"/g, "");
                frontendCss.answer['font-family'] = answer_final;
                $('.answer').css('font-family', style);
            },
            'fonts': [
                'Arial,Arial,Helvetica,sans-serif',
				'Arial Black,Arial Black,Gadget,sans-serif',
				'Comic Sans MS,Comic Sans MS,cursive',
				'Courier New,Courier New,Courier,monospace',
				'Georgia,Georgia,serif',
				'Impact,Charcoal,sans-serif',
				'Lucida Console,Monaco,monospace',
				'Lucida Sans Unicode,Lucida Grande,sans-serif',
				'Palatino Linotype,Book Antiqua,Palatino,serif',
				'Tahoma,Geneva,sans-serif',
				'Times New Roman,Times,serif',
				'Trebuchet MS,Helvetica,sans-serif',
				'Verdana,Geneva,sans-serif',
				'Gill Sans,Geneva,sans-serif'
            ]
        });
        // for the question font color
        $('#question_font').spectrum({
        	preferredFormat: "hex",
        	showInput: true,
        	showAlpha: true,
            showPalette: true,
            palette: [],
            color: questionfont_color,
            change: function(color) {
            	$('.question').css('color', color.toHexString());
            	frontendCss.question['color'] = color.toHexString();
    			color.toHexString(); // #ff0000
			}
        });

        $('#question_font').on('move.spectrum', function (e, tinycolor) {
            $('.question').css('color', tinycolor.toHexString());
            frontendCss.question['color'] = tinycolor.toHexString();
        });

        // for the question background color
        $('#question_background').spectrum({
        	preferredFormat: "hex",
        	showInput: true,
        	showAlpha: true,
            showPalette: true,
            palette: [],
            color: questionbg_color,
            change: function(color) {				
            	$('.question_panel').css('background', color.toHexString());
            	frontendCss.question_panel = {'background': color.toHexString()};				
    			color.toHexString(); // #ff0000
			}
            
        });

        $('#question_background').on('move.spectrum', function (e, tinycolor) {
            $(".question_panel").css('background', tinycolor.toHexString());
            frontendCss.question_panel = {'background': tinycolor.toHexString()};
			
        });

        // for the answer font color
        $('#answer_font').spectrum({
        	preferredFormat: "hex",
        	showInput: true,
        	showAlpha: true,
            showPalette: true,
            palette: [],
            color: answerfont_color,
            change: function(color) {
            	$('.answer').css('color', color.toHexString());
            	frontendCss.answer['color'] = color.toHexString();
    			color.toHexString(); // #ff0000
			}
                        
        });

        $('#answer_font').on('move.spectrum', function (e, tinycolor) {
            $('.answer').css('color', tinycolor.toHexString());
            frontendCss.answer['color'] = tinycolor.toHexString();
        });

        // for the answer background color
        $('#answer_background').spectrum({
        	preferredFormat: "hex",
        	showInput: true,
        	showAlpha: true,
            showPalette: true,
            palette: [],
            color: answerbg_color,
            change: function(color) {
            	$('.answer_panel').css('background', color.toHexString());
            	frontendCss.answer_panel['background'] = color.toHexString();
    			color.toHexString(); // #ff0000
			}
                        
        });

        $('#answer_background').on('move.spectrum', function (e, tinycolor) {
            $(".answer_panel").css('background', tinycolor.toHexString());
            frontendCss.answer_panel['background'] = tinycolor.toHexString();
        });       
    }, 1000);
    
    // for change arrows
    $('input:radio[name="arrow_pattern"]').change(function(){
        var arrow_change = $(this).val();
        ArrowCss['arrow_id'] = arrow_change;
        $.ajax({
            type: "GET",
            url: "{{ url('load/arrow') }}",
            data: 'id='+arrow_change,
            success: function (arrow_changed) {
                var arrowuptag = $('.arrow-up').attr('arrowup_tag');
                var arrowdowntag = $('.question_panel .arrow-down').attr('arrowdown_tag');
                var new_arrow = JSON.parse(arrow_changed);
                              
                if(arrowuptag == <?php echo '"'.$arrow->up.'"'; ?>)
                {
                    $('.question_panel .arrow-up').removeClass(<?php echo "'".$arrow->up."'"; ?>);
                    $('.question_panel .arrow-up').addClass(new_arrow.up);
                    $('.question_panel .arrow-up').attr('arrowup_tag',new_arrow.up);
                }
                else{
                    $('.question_panel .arrow-up').removeClass(arrowuptag);
                    $('.question_panel .arrow-up').addClass(new_arrow.up);
                    $('.question_panel .arrow-up').attr('arrowup_tag',new_arrow.up);
                }
                
                if(arrowdowntag == <?php echo '"'.$arrow->down.'"'; ?>)
                {
                    $('.question_panel .arrow-down').removeClass(<?php echo "'".$arrow->down."'"; ?>);
                    $('.question_panel .arrow-down').addClass(new_arrow.down);
                    $('.question_panel .arrow-down').attr('arrowdown_tag',new_arrow.down);
                }
                else{
                    $('.question_panel .arrow-down').removeClass(arrowdowntag);
                    $('.question_panel .arrow-down').addClass(new_arrow.down);
                    $('.question_panel .arrow-down').attr('arrowdown_tag',new_arrow.down);
                }
                    
            },
        });
    });
    
    //for arrow color arrow
    $('#arrow-color').spectrum({
    	preferredFormat: "hex",
    	showInput: true,
    	showAlpha: true,
        showPalette: true,
        palette: [],
        color: "<?php echo $arrow_css->arrow_color; ?>",
        change: function(color) {
        	$('.arrow').css('color', color.toHexString());
        	ArrowCss['arrow_color'] = color.toHexString();
			color.toHexString(); // #ff0000
		}
        
        });
    $('#arrow-color').on('move.spectrum', function (e, tinycolor) {
        $(".arrow").css('color', tinycolor.toHexString());
        ArrowCss['arrow_color'] = tinycolor.toHexString();
    });
    
    //for arrow position
    $('#arrow_position').on('change', function (e) {
        var arrow_location = $('#arrow_position').val();
        $('.arrow').css('float', arrow_location);
        ArrowCss['arrow_position'] = $('#arrow_position').val();
    });
        
    // ajax call to set the css from the database
    $.ajax({
        type: "GET",
        url: "{{ url('load/frontend/css') }}",
        success: function (styles) {
            items = JSON.parse(styles);
            $.each(items, function (key, value) {
                $('.' + key).css(value);
            });
        },
    });

    $("select#category").change(function () {
        $(this).find("option:selected").each(function () {
            var optionValue = $(this).attr("value");
            if (optionValue) {
                $(".questions").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else {
                $(".questions").hide();
            }
        });
    }).change();
    
     $('#border-color').spectrum({
     	preferredFormat: "hex",
    	showInput: true,
    	showAlpha: true,
        showPalette: true,
        palette: [],
        color: "<?php echo $border_color['color']; ?>",
        change: function(color) {
        	$('.question_panel').css('border-color', color.toHexString());
			frontendCss.border_panel = {'border-color': color.toHexString()};
			color.toHexString(); // #ff0000
		}
             
    });

    // change border color
    $('#border-color').on('move.spectrum', function (e, tinycolor) {
        $(".question_panel").css('border-color', tinycolor.toHexString());
        frontendCss.question_panel = {'border-color': tinycolor.toHexString()};
    });
    
    //border width
    $('#border-width').on('change', function (e) {
        
        var question_border = $('#border-width').val() + 'px solid <?php echo $border_color['color']; ?>';
        
        $('.question_panel').css('border', question_border);
        $('.question_panel').css('border-color', '<?php echo $border_color['color']; ?>');
        frontendCss.question_panel['border'] = $('#border-width').val() + 'px solid ';
    });
    
    //border Radius
    $('#border-radius').on('change', function (e) {
        var question_radius = $('#border-radius').val() + 'px';
        $('.question_panel').css('border-width', question_radius);
        frontendCss.question_panel['border-radius'] = $('#border-radius').val() + 'px';
    });
    
    //question Padding
    $('#queastion-padding').on('change', function (e) {
        var question_pad = $('#queastion-padding').val() + 'px';
        $('.question_panel').css('padding', question_pad);
        frontendCss.question_panel['padding'] = $('#queastion-padding').val() + 'px';
    });
    
    //answer Padding
    $('#answer-padding').on('change', function (e) {
        var answer_pad = $('#answer-padding').val() + 'px';
        $('.answer_panel').css('padding', answer_pad);
        frontendCss.answer_panel['padding'] = $('#answer-padding').val() + 'px';
    });
    
    //for Questions font size
    $('#question_fontsize').on('change', function (e) {
        if($('#question_fontsize').val() === '')
        {
            var question_font = "12px";
        }
        else{
            var question_font = $('#question_fontsize').val() + 'px';
        }
        $('.question').css('font-size', question_font);
        frontendCss.question['font-size'] = question_font;
    });
    
    //for Answer font size
    $('#answer_fontsize').on('change', function (e) {
        if($('#answer_fontsize').val() === '')
        {
            var answer_font = "12px";
        }
        else{
            var answer_font = $('#answer_fontsize').val() + 'px';
        }
        $('.answer').css('font-size', answer_font);
        frontendCss.answer['font-size'] = answer_font;
    });
   
    $('.question_panel').click(function (e) {
        e.preventDefault();

        var $this = $(this);

        $this.next().slideToggle("slow");
    });

    $('.save').click(function () {
		console.log(frontendCss.question);
        var createdcss = {
                "question":{
                    "font-family":("font-family" in frontendCss.question) ? frontendCss.question['font-family'] : question_font,
                    "color": ("color" in frontendCss.question) ? frontendCss.question['color'] : questionfont_color,                    
                    "font-size":("font-size" in frontendCss.question) ? frontendCss.question['font-size'] : $('#question_fontsize').val() + 'px', 
                }, 
                "answer":{
                    "font-family":("font-family" in frontendCss.answer) ? frontendCss.answer['font-family'] : font_answer,
                    "color":("color" in frontendCss.answer)? frontendCss.answer['color'] : answerfont_color,
                    "font-size":("font-size" in frontendCss.answer) ? frontendCss.answer['font-size'] : $('#answer_fontsize').val() + 'px'
                },
                "question_panel":{
                    "background":("background" in frontendCss.question_panel) ? frontendCss.question_panel['background'] : questionbg_color,
                    "border-radius":("border-radius" in frontendCss.question_panel) ? frontendCss.question_panel['border-radius'] : $("#border-radius-text").val() + 'px',
                    "padding":("padding" in frontendCss.question_panel) ? frontendCss.question_panel['padding'] : $("#question-padding-text").val() + 'px',
                    "border":("border" in frontendCss.question_panel) ? frontendCss.question_panel['border'] : $('#border-width').val() + 'px solid',
                    "border-color":("border-color" in frontendCss.border_panel) ? frontendCss.border_panel['border-color'] : <?php echo '"'.$border_color['color'].'"'; ?>
                },
                "answer_panel":{
                   "background":("background" in frontendCss.answer_panel) ? frontendCss.answer_panel['background'] : answerbg_color,
                   "padding":("padding" in frontendCss.answer_panel) ? frontendCss.answer_panel['padding'] : $("#answer-padding-text").val() + 'px'
                }
            }
            
           var arrow_css = {
                "arrow_id": (ArrowCss.arrow_id  === undefined) ? ArrowCss['arrow_id'] : $('input:radio[name="arrow_pattern"]:checked').val(),
                "arrow_color":(ArrowCss['arrow_color']) ? ArrowCss['arrow_color'] : <?php echo '"'.$arrow_css->arrow_color.'"'; ?>,
                "arrow_position":(ArrowCss.arrow_position  === undefined) ? ArrowCss['arrow_position'] : $('#arrow_position').val()
           }
        console.log(frontendCss.question_panel['background']+'=='+frontendCss.border_panel['border-color']);
        var json = JSON.stringify(createdcss);		
        var arrow_json = JSON.stringify(arrow_css);
        
        $.ajax({
            type: "POST",
            url: "{{ url('frontend/css') }}",
            data: {
                json: json,
                arrow_json: arrow_json,
                _token: $('[name="_token"]').val(),
            },
            success: function (msg) {
                location.reload();
            }
        });
        
    });

    //for Default css
    $('.default').click(function () {
        
        $.ajax({
            type: "POST",
            url: "{{ url('frontend/css/default') }}",
            data: {
                _token: $('[name="_token"]').val(),
            },
            success: function (msg) {
                
                location.reload();
            }
        });

    });

    /*Menu-toggle*/
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#sidebar").toggleClass("active_panel");
        $("#content").toggleClass("col-sm-6 col-sm-12");
    });

});
</script>
@endsection