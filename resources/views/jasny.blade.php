<!DOCTYPE html>
<html lang= "en">
<head>
  <title>Laravel</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{asset('js/fontselector/fontselect.css')}}">
  <link rel="stylesheet" href="{{ asset('js/colorpicker/spectrum.css') }}" type="text/css">
  <!-- shopify Script for fast load -->
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script>
    ShopifyApp.init({
      apiKey: 'd485b6049cd6b05fd39f50bc1857c65c',
      shopOrigin: 'https://zestard-technologies.myshopify.com'
    });

    ShopifyApp.ready(function() {
        ShopifyApp.Bar.initialize({
          icon: 'https://www.zestardshop.com/shopifyapp/multipageslider/zestard/assets/images/gallery.png',
          title: 'Management',
          buttons: { }
        });
      });
  </script>
  <style>
  /*!
   * Start Bootstrap - Simple Sidebar (http://startbootstrap.com/)
   * Copyright 2013-2016 Start Bootstrap
   * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
   */

   body {
      overflow-x: hidden;
   }

  /* Toggle Styles */

  #wrapper {
      padding-left: 0;
      -webkit-transition: all 0.5s ease;
      -moz-transition: all 0.5s ease;
      -o-transition: all 0.5s ease;
      transition: all 0.5s ease;
  }

  #wrapper.toggled {
      padding-left: 250px;
  }

  #sidebar-wrapper {
      z-index: 1000;
      position: fixed;
      left: 250px;
      width: 0;
      height: 100%;
      margin-left: -250px;
      overflow-y: auto;
      background: #000;
      -webkit-transition: all 0.5s ease;
      -moz-transition: all 0.5s ease;
      -o-transition: all 0.5s ease;
      transition: all 0.5s ease;
  }

  #wrapper.toggled #sidebar-wrapper {
      width: 250px;
  }

  #page-content-wrapper {
      width: 100%;
      position: absolute;
      padding: 15px;
  }

  #wrapper.toggled #page-content-wrapper {
      position: absolute;
      margin-right: -250px;
  }

  /* Sidebar Styles */

  .sidebar-nav {
      position: absolute;
      top: 0;
      width: 100%;
      margin: 0;
      padding: 0;
      list-style: none;
  }

  .sidebar-nav li {
      text-indent: 20px;
      line-height: 40px;
  }

  .sidebar-nav li a {
      display: block;
      text-decoration: none;
      color: #999999;
  }

  .sidebar-nav li a:hover {
      text-decoration: none;
      color: #fff;
      background: rgba(255,255,255,0.2);
  }

  .sidebar-nav li a:active,
  .sidebar-nav li a:focus {
      text-decoration: none;
  }

  .sidebar-nav > .sidebar-brand {
      height: 65px;
      font-size: 18px;
      line-height: 60px;
  }

  .sidebar-nav > .sidebar-brand a {
      color: #999999;
  }

  .sidebar-nav > .sidebar-brand a:hover {
      color: #fff;
      background: none;
  }

  @media(min-width:768px) {
      #wrapper {
          padding-left: 0;
      }

      #wrapper.toggled {
          padding-left: 250px;
      }

      #sidebar-wrapper {
          width: 0;
      }

      #wrapper.toggled #sidebar-wrapper {
          width: 250px;
      }

      #page-content-wrapper {
          padding: 20px;
          position: relative;
      }

      #wrapper.toggled #page-content-wrapper {
          position: relative;
          margin-right: 0;
      }
  }
    /* Set height of the grid so .sidenav can be 100% (adjust if needed)
    .row.content {
    }

    .container {
        overflow-y: hidden;
    }
    .page{
        margin: 0;
        padding: 0;
        height: 100vh;
        display : block;
        background : rgba(0,0,0,0.3);
        overflow-y: hidden;
    }
    .content  {
        padding: 0;
        height : 100%;
        overflow: auto;
    }
    */
    /* Set black background color, white text and some padding
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    */
    /* On small screens, set height to 'auto' for sidenav and grid
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;}
    }
    */.navbar-default .navbar-toggle {
  border-color: transparent; /* Removes border color */
  float: left; /* Move navbar toggle to left */
}
.navbar-default .navbar-toggle .icon-bar {
  background-color: #f00; /* Changes regular toggle color */
}
.navbar-default .navbar-toggle .icon-bar:hover {
  background-color: #fff; /* Changes toggle color on hover */
}
  </style>
</head>
<body>
  <div id="wrapper">

      <!-- Sidebar -->
      <div id="sidebar-wrapper">
          <ul class="sidebar-nav">


              <li>
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="option1">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" href="#question_design" aria-expanded="true" aria-controls="question_design">
                        Customize Question Design
                      </a>
                    </h4>
                  </div>
                  <div id="question_design" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="option1">
                    <div class="panel-body">
                      <h4>Customize font:</h4>
                        <input type="text" id="font" />
                      <br/>
                      <h4>Customize color:</h4>
                        Background <input type = "text" id = "question_background"/>
                        Font <input type = "text" id = "question_font"/>
                      <br/>
                      <br/>
                    </div>
                  </div>
                  </div>
              </li>
              <li>
                <div class="panel panel-primary">
                  <div class="panel-heading" role="tab" id="option2">
                    <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" href="#answer_design" aria-expanded="true" aria-controls="answer_design">
                        Customize Answer Design
                      </a>
                    </h4>
                  </div>

                  <div id="answer_design" class="panel-collapse collapse" role="tabpanel" aria-labelledby="option2">
                    <div class="panel-body">
                      <h4>Customize font:</h4>
                        <input type="text" id="font_answer" />
                      <br/>
                      <h4>Customize color:</h4>
                        Background <input type = "text" id = "answer_background"/>
                        Font <input type = "text" id = "answer_font"/>
                      <br/>
                      <br/>
                    </div>
                  </div>
                </div>
              </li>
              <li>
                  <a href="#">Overview</a>
              </li>
              <li>
                  <a href="#">Events</a>
              </li>
              <li>
                  <a href="#">About</a>
              </li>
              <li>
                  <a href="#">Services</a>
              </li>
              <li>
                  <a href="#">Contact</a>
              </li>
          </ul>
      </div>
      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12">
                      <h1>Simple Sidebar</h1>
                      <p>This template has a responsive menu toggling system. The menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will appear/disappear. On small screens, the page content will be pushed off canvas.</p>
                      <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>.</p>
                      <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                  </div>
              </div>
          </div>
      </div>
      <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->
<script src="{{ asset('js/3.2.1.jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/fontselector/jquery.fontselect.js') }}"></script>
<script src="{{ asset('js/colorpicker/spectrum.js') }}"></script>

<script type = "text/javascript">
$(document).ready(function() {
  $("select#category").change(function(){
    $(this).find("option:selected").each(function(){
        var optionValue = $(this).attr("value");
        if(optionValue){
            $(".questions").not("." + optionValue).hide();
            $("." + optionValue).show();
        } else{
            $(".questions").hide();
        }
    });
  }).change();

    $('#question_font').spectrum({
      showPalette: true,
      palette: [ ],
      showSelectionPalette: true, // true by default
      selectionPalette: ["red", "green", "blue"],
      maxSelectionSize: 2
    });

    $('#question_font').on('move.spectrum' , function(e, tinycolor) {
        $('.question').css('color' , tinycolor.toHexString() );
    });

    $('#question_background').spectrum({
      showPalette: true,
      palette: [ ],
      showSelectionPalette: true, // true by default
      selectionPalette: ["red", "green", "blue"],
      maxSelectionSize: 2
    });

    $('#question_background').on('move.spectrum' , function(e, tinycolor) {
        $(".question-panel").css('background',tinycolor.toHexString());
    });


    $('#answer_font').spectrum({
      showPalette: true,
      palette: [ ],
      showSelectionPalette: true, // true by default
      selectionPalette: ["red", "green", "blue"],
      maxSelectionSize: 2
    });

    $('#answer_font').on('move.spectrum' , function(e, tinycolor) {
        $('.answer').css('color' , tinycolor.toHexString() );
    });

    $('#answer_background').spectrum({
      showPalette: true,
      palette: [ ],
      showSelectionPalette: true, // true by default
      selectionPalette: ["red", "green", "blue"],
      maxSelectionSize: 2
    });

    $('#answer_background').on('move.spectrum' , function(e, tinycolor) {
        $(".answer-panel").css('background',tinycolor.toHexString());
    });

  $('#font').fontselect().change(function(){

  // replace + signs with spaces for css
  var font = $(this).val().replace(/\+/g, ' ');

  // split font into family and weight
  font = font.split(':');

  // set family on paragraphs
    $('.question').css('font-family', font[0]);
  });


  $('#font_answer').fontselect().change(function(){

  // replace + signs with spaces for css
  var font = $(this).val().replace(/\+/g, ' ');

  // split font into family and weight
  font = font.split(':');

  // set family on paragraphs
    $('.answer').css('font-family', font[0]);
  });

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});


});
</script>
</body>
</html>
