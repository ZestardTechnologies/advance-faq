@if(count($questions_array) > 0)
@foreach($questions_array as $id=>$question)
<ul class="accordion">
  <li>
    <a class="toggle" href="javascript:void(0);">{{ $question['question'] }}</a>
    <ul class="inner">
      <li>{{ $question['answer'] }}</li>
    </ul>
  </li>
</ul>
@endforeach
@endif
