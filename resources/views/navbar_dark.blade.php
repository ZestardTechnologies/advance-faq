<!--navbar-->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<div class="container-fluid">
<div class="navbar-header">
          <div  class="navbar-brand">
              <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                  <i class="fa fa-bars"></i>
              </a>
              <a href="#">Project name</a>
          </div>
</div>
<div id="navbar" class="collapse navbar-collapse">
  <ul class="nav navbar-nav">
    <li @if($active == 'manage')class="active" @endif><a href="{{ route('dashboard') }}">Manage</a></li>
    <li @if($active == 'topic')class="active"@endif><a href="{{ route('topic') }}">Topic</a></li>
    <li @if($active == 'design')class="active"@endif><a href="{{ route('design') }}">Design</a></li>
    <li @if($active == 'qa')class="active"@endif><a href="{{ route('qalist') }}">Q/A</a></li>
     <li @if($active == 'help')class="active" @else class="" @endif><a href="{{ route('help') }}">Help?</a></li>
  </ul>
</div><!--/.nav-collapse -->
</div>
</nav>
<!-- end navbar-->
