@extends('header')

@section('content')
<div class="container formcolor">
  <div class="row">
      <div class="container">
      <div class="col-sm-6 page-left-section">
      <h3 class="ribbon">Question list</h3>
    </div>
    <div class="col-sm-6 add-right-button">
      <a href="{{ url('addqa') }}" class="btn btn-primary">Add QUESTION</a>
    </div>
  <table id="topiclist" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
                <th style="width:10px !important;">Id</th>
                <th>Question</th>
                <th>Answer</th>
                <th>Topic</th>
                <th style="width:10px !important;" class="status-shorting">Status</th>
                <th style="width:10px !important;">Order</th>
                <th style="width:65px !important;" class="action-shorting">Action</th>
            </tr>
          </thead>
          <tbody>
              <?php $Qi = 1; ?>
            @foreach($qa as $id => $qalist)
              <tr>
                  <td>{{ $Qi }}</td>
                  <td><?php $str=$qalist['question']; if(strlen($str) > 200){ echo substr($str,0,150)."..."; }else{echo $str;} ?></td>
                  <td><?php $strans=$qalist['answer']; if(strlen($strans) > 200){ echo substr(strip_tags($strans),0,150)."..."; }else{echo strip_tags($strans);} ?> </td>
                  <td>@if($qalist['topic_id'] == 0)<?php echo"Uncategorised"; ?>@else <?php echo $qalist['topic_name']; ?>@endif</td>
                  <td>@if($qalist['qa_status'] == 0)<?php echo"Active";?>@elseif($qalist['qa_status'] == 1)<?php echo"Deactive";?>@endif</td>
                  <td>{{ $qalist['qa_order'] }}</td>
                  <td><a href="{{ url('editqa/'.$qalist['encrypt_id']) }}"><span class="glyphicon glyphicon-edit"></span></a>|
                  <a href="{{ url('deleteqa/'.$qalist['encrypt_id']) }}" onclick="return confirm('Are you sure you want to delete this Question?');"><span class="glyphicon glyphicon-trash"></span></a></td>
              </tr>
              <?php $Qi++; ?>
            @endforeach
          </tbody>
      </table>
    </div>
      </div>
</div>
@endsection
